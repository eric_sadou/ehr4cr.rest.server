
<h2>EHR4CR Rest Services</h2>

<a href="static/EHR4CR_Services0.1.pdf">Documentation (v 0.1)</a>
<p>
	<img src="static/ObjectDiagram.png" />
</p>
<p>&nbsp;</p>
<p>
	<img src="static/ObjectDiagram2.png" />
</p>

<h3>Context Elements Services (Structure of a Data Element for
	workbench)</h3>
<table>
	<tr>
		<td><a href="rest/context">link</a></td>
		<td>Get Root Node of Context Elements.</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a href="rest/context/xml">link</a></td>
		<td>Get Root Node of Context Elements.</td>
		<td>xml</td>
	</tr>

	<tr>
		<td><a
			href="rest/context/element?uri=http://server.ubiqore.com/v1/DataElement/EHR4CR_VS_1.6">link</a></td>
		<td>Access example element with internal URI</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a
			href="rest/context/xml/element?uri=http://server.ubiqore.com/v1/DataElement/EHR4CR_VS_1.6">link</a></td>
		<td>Access example element with internal URI</td>
		<td>xml</td>
	</tr>

	<tr>
		<td><a href="rest/context/search?pattern=ecog">link</a></td>
		<td>Search <i>string pattern</i> from element label
		</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a href="rest/context/xml/search?pattern=ecog">link</a></td>
		<td>Search <i>string pattern</i> from element label
		</td>
		<td>xml</td>
	</tr>

</table>

<h3>Pivot (Multi-Terminology) Services</h3>
<table>
	<tr>
		<td><a href="rest/pivot">link</a></td>
		<td>Get Pivot Terminology (schemes object)</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a href="rest/pivot/xml">link</a></td>
		<td>Get Pivot Terminology (schemes object)</td>
		<td>xml</td>
	</tr>
	<tr>
		<td><a href="rest/pivot/2.16.840.1.113883.6.3/B01">link</a></td>
		<td>Get A term from officials schemeOid/code (<i>direct</i>
			Narrowers included)
		</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a href="rest/pivot/xml/2.16.840.1.113883.6.3/B01">link</a></td>
		<td>Get A term from officials schemeOid/code (<i>direct</i>
			Narrowers included)
		</td>
		<td>xml</td>
	</tr>

	<tr>
		<td><a
			href="rest/pivot/getTerm?uri=http://server.ubiqore.com/v1/2.16.840.1.113883.6.3%23C00-C97.9">link</a></td>
		<td>Get A term from internal uri (<i>direct</i> Narrowers
			included)
		</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a
			href="rest/pivot/xml/getTerm?uri=http://server.ubiqore.com/v1/2.16.840.1.113883.6.3%23C00-C97.9">link</a></td>
		<td>Get A term from internal uri (<i>direct</i> Narrowers
			included)
		</td>
		<td>xml</td>
	</tr>

	<tr>
		<td><a
			href="rest/pivot/getTermWithSubLevel?uri=http://server.ubiqore.com/v1/2.16.840.1.113883.6.3%23C00-C97.9&sublevel=4">link</a></td>
		<td>Get A Term Definition with <i>transitive</i> Narrowers Links
			(4 <i>(as example)</i> sub levels in parameter )
		</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a
			href="rest/pivot/xml/getTermWithSubLevel?uri=http://server.ubiqore.com/v1/2.16.840.1.113883.6.3%23C00-C97.9&sublevel=4">link</a></td>
		<td>Get A Term Definition with <i>transitive</i> Narrowers Links
			(4 <i>(as example)</i>sub levels as parameter )
		</td>
		<td>xml</td>
	</tr>


	<tr>
		<td><a
			href="rest/pivot/searchSubTerms?pattern=hiv&uri=http://server.ubiqore.com/v1/2.16.840.1.113883.6.3%23B20-B24.9">link</a></td>
		<td>search term from string pattern inside the sub terms ot a
			term.</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a
			href="rest/pivot/xml/searchSubTerms?pattern=hiv&uri=http://server.ubiqore.com/v1/2.16.840.1.113883.6.3%23B20-B24.9">link</a></td>
		<td>search term from string pattern inside the sub terms ot a
			term.</td>
		<td>xml</td>
	</tr>
	<tr>
		<td><a
			href="rest/pivot/exportRDF?uri=http://server.ubiqore.com/v1/2.16.840.1.113883.6.3%23B20-B24.9">link</a></td>
		<td>Download RDF subset from a concept</td>
		<td>(rdf file)</td>
	</tr>

</table>

<h3>Mapping & Mapping Process Material Download Services.</h3>
<table>
	<tr>
		<td><a
			href="rest/pivot/getErsatzConcept?value=C00-C97.9&codingSchemeURI=urn:oid:2.16.840.1.113883.6.3">link</a></td>
		<td>GetErsatzConcept (for mapping loader)</td>
		<td><b>json</b></td>
	</tr>
	<tr>
		<td><a href="rest/pivot/download">link</a></td>
		<td>Get The Original/simple terms List for Mapping (October
			2013).*</td>
		<td>(original.tsv) zip</td>
	</tr>
	<tr>
		<td><a href="rest/pivot/download?type=FULL">link</a></td>
		<td>Get full (original+ full sub terms) List (October 2013).*</td>
		<td>(full.tsv) zip</td>
	</tr>
	<tr>
		<td><a href="rest/pivot/download?type=levels&sublevels=3">link</a></td>
		<td>Get The list with 3<i>(as example)</i> sublevels from
			original list.*
		</td>
		<td>(levels.tsv) zip</td>
	</tr>
	<tr>
		<td><a href="rest/pivot/download?type=EXPANDED">link</a></td>
		<td>Get List of all concepts inside pivot terminology.* & **</td>
		<td>(expanded.tsv) zip</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><i>* (2 lines of each "CO" Concept--> One of the line
				include the value as part of the code.))</i></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><i>** [included : full LOINC, ATC , ICD-10 codes / 5 mn
				process / 20 Mo zipped tsv file is generated]</i></td>
	</tr>

</table>

