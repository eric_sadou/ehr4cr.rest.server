'use strict';



// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', function($routeProvider) {
   // $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: HomeCtrl});
   // $routeProvider.when('/view2', {templateUrl: 'partials/partial2.html', controller: MyCtrl2});
   // $routeProvider.when('/properties', {templateUrl: 'partials/properties.html', controller: PropertiesCtrl});
   // $routeProvider.when('/configurations', {templateUrl: 'partials/configurations.html', controller: ConfigurationsCtrl});
   // $routeProvider.when('/current', {templateUrl: 'partials/current.html', controller: CurrentCtrl});
    $routeProvider.when('/central', {templateUrl: 'partials/central.html', controller: CentralCtrl});
    $routeProvider.when('/central2', {templateUrl: 'partials/centralBis.html', controller: CentralCtrl2});
    $routeProvider.when('/central3', {templateUrl: 'partials/centralTer.html', controller: CentralCtrl3});
    $routeProvider.when('/term', {templateUrl: 'partials/term.html', controller: TermCtrl});
    $routeProvider.when('/test', {templateUrl: 'test.jsp', controller: TestCtrl});
    $routeProvider.otherwise({redirectTo: '/central'});
  }]);
  
  
  

