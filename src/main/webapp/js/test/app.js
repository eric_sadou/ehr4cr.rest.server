'use strict';



// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/home', {templateUrl: 'partials/test/home.html', controller: csearch});
    $routeProvider.otherwise({redirectTo: '/home'});
  }]);
  
  
  

