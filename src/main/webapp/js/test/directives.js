'use strict';

/* Directives */


angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])
  .directive('tooltip', function() {
	  return {
	        restrict:'A',
	        link: function(scope, element, attrs)
	        {   
	        	var str=scope.$eval(attrs.tooltip);
	        	
	        	
	        	console.log(str);
	        	if (str.length >= 50){
	        		$(element).text(str.substring(0,48)+"...");
	        		$(element).attr('title',scope.$eval(attrs.tooltip));
	        		$(element).tooltip({placement: "bottom"});
	        	}else {
	        		$(element).text(str);
	        	}
	        	
	           
	        }
	    }  })
  .directive('currentTime', function($timeout, serviceTime) {
	    // return the directive link function. (compile function not needed)
	    return function(scope, element, attrs) {
	      var timeoutId; // timeoutId, so that we can cancel the time updates

	      // used to update the UI
	      function updateTime() {
	    	serviceTime.getTime().then(function(response){
	    		element.text(response.data);
	    	});
	    	
	        
	      }

	      // watch the expression, and update the UI on change.
	      scope.$watch(attrs.currentTime, function() {
	        updateTime();
	      });

	      // schedule update in one second
	      function updateLater() {
	        // save the timeoutId for canceling
	        timeoutId = $timeout(function() {
	          updateTime(); // update DOM
	          updateLater(); // schedule another update
	        }, 1000000);
	      }

	      // listen on DOM destroy (removal) event, and cancel the next UI update
	      // to prevent updating time ofter the DOM element was removed.
	      element.bind('$destroy', function() {
	        $timeout.cancel(timeoutId);
	      });

	      updateLater(); // kick off the UI update process.
	    }
	  })
	  .directive('fileUpload', function() {
	    // return the directive link function. (compile function not needed)
	    return {
	    	restrict : 'E',
	    	replace: true,
	    	template: 
	    	      '<form class="form-horizontal" method="post" enctype="multipart/form-data" ng-submit="submit()">' +
	    	      '<div class="control-group"><div class="controls"> <input type="file" id="ufile" name="content" multiple  /></div></div>' +
	    	      '<div class="form-actions"><input class="btn  btn-primary" type="submit" value="Upload" /></div></form>',
	    	controller: function ($scope, $element ,$http){
	    		$scope.submit = function (){
	    			$scope.freeze=true;
	    			//var fileInput = $element.find('input[type="file"]');
	    			var fileInput = document.getElementById('ufile');
	    			var file = fileInput.files[0];
	    			if (file==null){
	    				if ($scope.minimessage=="no file selected")$scope.minimessage="still now, no file selected";
	    				else $scope.minimessage="no file selected";
	    				
	    				$scope.freeze=false;
	    				return;
	    				}
	    			$scope.minimessage=""; // no more issue with file to select...
	    			var formData = new FormData();
	    			console.log(fileInput);
	    			//var formData = new FormData();
	    			formData.append('content', file);
	    			console.log(formData);
	    			$http({method: 'POST', url: 'services/rest/mappingservices/upload', data: formData, headers: {'Content-Type': undefined }, transformRequest: angular.identity})
	    			.success(function(data, status, headers, config) {
	    				$scope.bootstrapAlertState="alert"; 
	    				$scope.status={code :'OK' , message :"upload was successfull / check conf button to verify.."};
	    				
	    				$scope.report=data;
	    				$scope.freeze=false;
	    				
	    				console.log($scope.report);
	    			});
	    			/*$.post( 'services/rest/mappingservices/upload',
	    							function(){
	    									$scope.$emit('uploadDone', {success: true});
	    									  }
	    				  ).error(  function(){
	    					  				$scope.$emit('uploadDone', {success: false});
	    				  					});*/
	    			};
	    		},
	    		link: function (scope, elem, attrs ){}
	    	};
	  })
	  /* .directive('treatmentPercent', function($timeout,servicePercent) {
	    // return the directive link function. (compile function not needed)
	    return function(scope, element, attrs) {
	      var timeoutId; // timeoutId, so that we can cancel the time updates
	   
	      // used to update the UI
	      function updatePercent() {
	    	  servicePercent.getPercent().then(function(response){
	    		  if (response.data=="end"){
	    			  $timeout.cancel(timeoutId);
	    			 // alert("end of treatment");
	    		  }
	    		  else element.css('width',response.data+'%');
	    		 
		    	});
	    		
	    	
	      }

	      // watch the expression, and update the UI on change.
	      scope.$watch(attrs.treatmentPercent, function() {
	    	  alert('watch');
	    	  upldateLater();
	      });

	      // schedule update in one second
	      function updateLater() {
	        // save the timeoutId for canceling
	        timeoutId = $timeout(function() {
	          updatePercent(); // update DOM
	          updateLater(); // schedule another update
	        }, 5000);
	      }

	      // listen on DOM destroy (removal) event, and cancel the next UI update
	      // to prevent updating time ofter the DOM element was removed.
	      element.bind('$destroy', function() {
	        $timeout.cancel(timeoutId);
	      });

	       // kick off the UI update process.
	      //updateLater();
	    }
	  });*/
	  ;
  
