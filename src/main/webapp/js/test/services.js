'use strict';

/* Services */

// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', []).value('version', '0.7').factory(
		'serviceTime', [ '$http', function($http) {
			function serviceTimeServc() {

				this.getTime = function() {

					var promise = $http({
						method : "GET",
						datatype : "json",
						url : 'services/rest/mappingservices/getTime'
					}).success(function(data, status) {
						console.log(data);
						console.log(status);

					});

					return promise;

				}
			}
			;
			return new serviceTimeServc();
		} ]).factory(
				'percentService', [ '$http', function($http) {
					function percentServc() {

						this.getPercent = function() {

							var promise = $http({
								method : "GET",
								datatype : "json",
								url : 'services/rest/mappingservices/percent'
							}).success(function(data, status) {
								console.log(data);
								console.log(status);

							});

							return promise;

						}
					}
					;
					return new percentServc();
				} ]).factory('propertiesService', [ '$http', function($http) {
	function propertiesServc() {

		this.getIt = function() {

			var promise = $http({
				method : "GET",
				datatype : "json",
				url : 'services/rest/manager/properties'
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			});

			return promise;

		}
	}
	;
	return new propertiesServc();
} ]).factory('statusService', [ '$http', function($http) {
	function statusServc() {

		this.getStatus = function() {

			var promise = $http({
				method : "GET",
				datatype : "json",
				url : 'services/rest/manager/status'
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			});

			return promise;

		}
	}
	;
	return new statusServc();
} ]).

factory('initService', [ '$http', function($http) {
	function initServc() {

		this.init = function() {

			var promise = $http({
				method : "GET",
				datatype : "json",
				url : 'services/rest/manager/initSystem'
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			});

			return promise;

		}
	}
	;
	return new initServc();
} ]).factory('createService', [ '$http', function($http) {
	function createServc() {

		this.create = function(label) {

			var promise = $http({
				method : "GET",
				params : { label: label },
				datatype : "json",
				url : 'services/rest/mappingservices/createMapping'
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			});

			return promise;

		}
	}
	;
	return new createServc();
} ]).factory('configurationsService', [ '$http', function($http) {
	function configurationsServc() {

		this.getIt = function() {

			var promise = $http({
				method : "GET",
				datatype : "json",
				url : 'services/rest/manager/configurations'
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			});

			return promise;

		}
	}
	;
	return new configurationsServc();
	
} ]).factory('currentService', [ '$http', function($http) {
	function currentServc() {

		this.getIt = function() {

			var promise = $http({
				method : "GET",
				datatype : "json",
				url : 'services/rest/mappingservices/getLocals'
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			});

			return promise;

		}
	}
	;
	return new currentServc();
} ]).factory('localMappingService', [ '$http', function($http) {
	function localMappingServiceServc() {
		
		this.getMapping = function(code) {

			var promise = $http({
				method : "GET",
				params : { code: code },
				datatype : "json",
				url : 'services/rest/mappingservices/getMappingFromLocalCode'
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			});

			return promise;

		}
	}
	;
	return new localMappingServiceServc();
} ]).factory('searchLocalService', [ '$http', function($http) {
	function searchLocalServiceServc() {
		
		this.search = function(pattern) {

			var promise = $http({
				method : "GET",
				params : { pattern: pattern },
				datatype : "json",
				url : 'services/rest/mappingservices/searchLocal'
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			});

			return promise;

		}
	}
	;
	return new searchLocalServiceServc();
} ])
.factory('restService', [ '$http', function($http) {
	function restServc() {
		
		this.execute = function(paramsArray,url) {

			var promise = $http({
				method : "GET",
				params : paramsArray ,
				datatype : "json",
				url : url
			}).success(function(data, status) {
				console.log(data);
				console.log(status);

			}).error(function(data, status) {
				alert(status);

			});

			return promise;

		}
	}
	;
	return new restServc();
} ])
;

