'use strict';

function csearch($http, $rootScope,$scope,restService,$log){
	
	
	
	$scope.restsearch = function (){
		
		
		if ($scope.pattern.length <= 0){
			$scope.result=null;
			$scope.searchMessage="No Input";
			return;
		}
		var scheme='none';
		if ($scope.schemeSelected!=null){
			if ($scope.schemeSelected.uri!=null){
				scheme=$scope.schemeSelected.uri;
			}
		}
		var paramsArray={pattern:$scope.pattern , mode:'label',schemeUri:scheme ,type:'auto' };
		
		var url='rest/pivot/searchTerms';
		$scope.workInprogress=true;
		$scope.searchMessage=null;
		restService.execute(paramsArray,url).then(function(response){
			$scope.workInprogress=false;
			if (response.data.info1 !=null){
				// means no result
				$scope.searchMessage="No result in the terminology subsets";
			}
			else{ 
				   $scope.result=response.data;
				   $log.log($scope.result);
				   
			}
		});
	};
	
	
}
csearch.$inject = ['$http','$rootScope','$scope','restService','$log'];
