'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }]).
  filter('uriLastElement',function() {
	    return function(text) {
	    if (text==null)return null;
	    return text.substr(text.lastIndexOf('#') + 1);
	    };
	  }).
  filter('labelFilter',function() {
		    return function(text) {
		    if (text==null)return null;
		    if (text.length <50)return text;
		    else return text.substring(0,48)+" ...";
		    };
		  }).
  filter('goodtab',function() {
		    return function(nb) {
		    if (nb==null)return null;
		    var tab="";
		    for (var i=1;i<=nb;i++)
		    {
		    	tab+="\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0";
		    }
		    return tab;
		    
		    };
		  })
  ;
