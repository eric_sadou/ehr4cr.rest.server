'use strict';

function TermCtrl($http, $rootScope,$scope,restService){
	$http.defaults.useXDomain=true;
	$scope.pattern="";
	$scope.result=null;
	$rootScope.header='term';
	$scope.showSearch=true;
	$scope.workInprogress=false;
	$scope.searchMessage=null;
	$scope.mode='label';
	
	var urlSchemes='rest/pivot';
	restService.execute(null,urlSchemes).then(function(response){
		 
		$scope.schemes=response.data;
		var all={label:'All Terminologies'};
		$scope.schemes.unshift(all);
		$scope.schemeSelected=$scope.schemes[0];
	});
	$scope.search = function (){
		
		$scope.searchMessage=null;
		if ($scope.pattern.length <= 0){
			$scope.result=null;
			$scope.searchMessage="No Input";
			return;
		}
		var scheme='none';
		if ($scope.schemeSelected!=null){
			if ($scope.schemeSelected.uri!=null){
				scheme=$scope.schemeSelected.uri;
			}
		}
		var paramsArray={pattern:$scope.pattern , mode:$scope.mode,schemeUri:scheme  };
		
		var url='rest/pivot/searchTerms';
		$scope.workInprogress=true;
		restService.execute(paramsArray,url).then(function(response){
			$scope.workInprogress=false;
			if (response.data.info1 !=null){
				// means no result
				$scope.searchMessage="No result in the terminology subsets";
			}
			else $scope.result=response.data;
			
		});
	};
	
	$scope.searchFromCode = function (){
		$scope.mode="code";
		$scope.search();
	};
	$scope.searchFromLabel= function (){
		$scope.mode="label"; 
		$scope.search();
	};
	
	
	$scope.showRoot= function (){
		$scope.searchMessage=null;
		$scope.pattern="";
		$scope.result=null;
		$scope.zoom=null;
		$scope.zoom2=null;
		// clean up :
		$scope.zoomOnScheme=null;
		$scope.MyFathers=null;
		$scope.MyTerm =null;
		$scope.terms= null;
		$scope.MyDownTerm=null;
		
		var paramsArray={ };
		
		var url='rest/pivot';
		restService.execute(paramsArray,url).then(function(response){
			
			$scope.root=response.data;
			$scope.rootBU=$scope.root;
			$scope.workInprogress=false;
			
			return 'toto';
		});
	};
	
	$scope.showTermDetails= function (term){
		$scope.zoom=term;
	};
	
	$scope.showSchemeDetails= function (scheme){
		$scope.zoom2=scheme;
	};
	
	$scope.unShow = function (){
		$scope.zoom=null;
		$scope.zoom2=null;
	};
		
	$scope.showTopTerms = function (scheme){
		$scope.zoomOnScheme=scheme;
		$scope.MyTerm=null;
		$scope.terms=scheme.topTerms;
	};
	
	$scope.showSubTerms = function (term){
		$scope.result=null;
		$scope.root=null;
		$scope.MyDownTerm=null;
		$scope.MyFathers=null;
		$scope.zoom=null;
		$scope.zoom2=null;
		 
		var paramsArray={uri:term.uri };
		var url='rest/pivot/getTerm';
		restService.execute(paramsArray,url).then(function(response){
			//if (response.data.narrowers.length >0){
				$scope.MyTerm=response.data;
				$scope.terms=$scope.MyTerm.narrowers;
			//}
			
		});	
	};
	
	$scope.showUpperTerms = function (term){
		$scope.root=null;
		
		$scope.zoom=null;
		$scope.zoom2=null;
		
		$scope.MyDownTerm=null; 
		var paramsArray={uri:term.uri };
		var url='rest/pivot/getDirectSupTerms';
		restService.execute(paramsArray,url).then(function(response){
			$scope.terms=null;
			$scope.MyTerm=null;
		
			$scope.MyFathers=response.data;
			if ($scope.MyFathers.length ==0){
				var scheme =$scope.zoomOnScheme;
				$scope.root=$scope.rootBU;
				$scope.showTopTerms(scheme);
			}else {
				$scope.MyDownTerm=term;
			}
		});	
	};
	
	
	// init
	$scope.showRoot();
}
TermCtrl.$inject = ['$http','$rootScope','$scope','restService'];
function CentralCtrl($rootScope,$scope,restService){
	$rootScope.header='central';
	$scope.zoomOn=null;
	$scope.workInprogress=true;
	$scope.showed=null;
	$scope.termView=null;
	$scope.termHistory=null;
	$scope.hideView=true;
	
	var paramsArray={ };
	var url='rest/context';
		restService.execute(paramsArray,url).then(function(response){
			$scope.root=response.data;
			$scope.workInprogress=false;
		});
	
	$scope.showTerm = function(subTerm){
		$scope.hideView=true;
		$scope.zoomOnSubTerm=subTerm;
		$scope.showedSubTerm=subTerm.uri;
	}
	
	$scope.unShowTerm = function(subTerm){
		$scope.hideView=false;
		$scope.zoomOnSubTerm=null;
	}
	
	$scope.show = function (element){
		//$scope.termView=null;
		$scope.zoomOnSubTerm=null;
		$scope.showedSubTerm=null;
		$scope.hideView=true;
		$scope.zoomOn=element;
		if ($scope.showed!=null)$scope.tmp=$scope.showed;
		$scope.showed=element.uri;
		
	};
	
	$scope.tmp=null;
	
	$scope.unshow = function (element){
		$scope.hideView=false;
		$scope.zoomOn=null;
		$scope.showed=$scope.tmp;
	};
	
	$scope.unshowForLeaf = function (element){
		$scope.hideView=false;
		$scope.zoomOn=null;
		if ($scope.termView!=null)$scope.showed=element.uri;
		else $scope.showed=null;
	};
	
	// subTerm is an element inside the history array.
	$scope.showSubTerms= function (uriElement, uriRelated,subTerm) {
		$scope.hideView=false;
		$scope.zoomOnSubTerm=null;
		$scope.showedSubTerm=null;
		
		if (uriElement!=null){
			$scope.showed=uriElement;
			$scope.termHistory=null;
		}
		
		
		$scope.workInprogress=true;
		var paramsArray={uri: uriRelated };
		var url='rest/pivot/getTerm';
		
		restService.execute(paramsArray,url).then(function(response){
				
				$scope.zoomOn=null;
				$scope.termView=response.data;
				
				if (uriElement==null){
					if ($scope.termHistory==null){
						$scope.termHistory=new Array();
						$scope.termHistory[0]=response.data;
					}else {
						if (subTerm!=null){
							var i=$scope.termHistory.indexOf(subTerm);
							
							for (var j=i;j<$scope.termHistory.length;j=j+1){
								$scope.termHistory.splice(j);
							}
						}
						$scope.termHistory.push(response.data);
						
					}
				}
				$scope.workInprogress=false;
			});
	};
	
	$scope.explore = function (uriSelected){
		if (uriSelected==null)return;
		$scope.termHistory=null;
		$scope.hideView=false;
		$scope.showed=null;
		$scope.termView=null;
		$scope.workInprogress=true;
		var paramsArray={uri: uriSelected };
		var url='rest/context/element';
			restService.execute(paramsArray,url).then(function(response){
				
				//$scope.root=$scope.explored;
				$scope.root=response.data;
				$scope.zoomOn=null;
				$scope.workInprogress=false;
			});
	};
	
} 

CentralCtrl.$inject = ['$rootScope','$scope','restService'];


function CentralCtrl2($rootScope,$scope,restService){
	$rootScope.header='central2';
	$scope.zoomOn=null;
	$scope.workInprogress=true;
	$scope.showed=null;
	$scope.termView=null;
	$scope.termHistory=null;
	$scope.hideView=true;
	$scope.relatedSchemes=null;
	$scope.experimental=false;
	var paramsArray={ };
	
	$scope.cleanupTheView= function (){
		$scope.relatedSchemes=null;
		$scope.focusOnScheme=null;
		
		$scope.specialSubTerms=null;
		$scope.specialElements=null;
		$scope.specialTermHistory=null;
	};
	
	
	var url='rest/context';
		restService.execute(paramsArray,url).then(function(response){
			$scope.root=response.data;
			$scope.workInprogress=false;
		});
	
	$scope.showTerm = function(subTerm){
		$scope.hideView=true;
		$scope.zoomOnSubTerm=subTerm;
		$scope.showedSubTerm=subTerm.uri;
	};
	
	$scope.unShowTerm = function(subTerm){
		$scope.hideView=false;
		$scope.zoomOnSubTerm=null;
	}
	
	$scope.show = function (element){
		//$scope.termView=null;
		$scope.zoomOnSubTerm=null;
		$scope.showedSubTerm=null;
		$scope.hideView=true;
		$scope.zoomOn=element;
		if ($scope.showed!=null)$scope.tmp=$scope.showed;
		$scope.showed=element.uri;
		
	};
	
	$scope.tmp=null;
	
	$scope.unshow = function (element){
		$scope.hideView=false;
		$scope.zoomOn=null;
		$scope.showed=$scope.tmp;
	};
	
	$scope.unshowForLeaf = function (element){
		$scope.hideView=false;
		$scope.zoomOn=null;
		if ($scope.termView!=null)$scope.showed=element.uri;
		else $scope.showed=null;
	};
	
	$scope.showSpecialTopTerms= function (scheme){
		$scope.focusOnScheme=scheme;
		$scope.specialTermHistory=null;
		$scope.specialElements=null;
		return $scope.showSpecialSubTerms(scheme,false);
	};
	
	$scope.showSpecialSubTermsComingFromHistory=function (element){
		
			var i=$scope.specialTermHistory.indexOf(element);
			
			for (var j=i;j<$scope.specialTermHistory.length;j=j+1){
				$scope.specialTermHistory.splice(j);
			}
		
			
		
		return $scope.showSpecialSubTerms(element,true);
	};
	
	$scope.showSpecialSubTerms=function (element,notAScheme){
		
		if (notAScheme==null)notAScheme=true;
		
		$scope.experimental=true;
		$scope.specialSubTerms=null;
		$scope.termHistory=null;
		$scope.termView=null;
		$scope.showed=null;
		$scope.specialElements=null;
		
		if (notAScheme==true){
			if ($scope.specialTermHistory==null){
				
				$scope.specialTermHistory=new Array();
			}
			$scope.specialTermHistory.push(element);
			console.log("SpecialTermhistory="+ $scope.specialTermHistory);
		}
		
		var paramsArray={elementUri: $scope.root.uri , schemeOrTermUri: element.uri   };
		var url='rest/context/validateSupTermsForMembers';
		restService.execute(paramsArray,url).then(function(response){
			
					
					if (response.data.info1 !=null){
						// do nothing ...
					}
					else {
						
						
						
						$scope.specialSubTerms=null;
						// $scope.relatedSchemes=null;
						$scope.specialSubTerms=new Array();
						$scope.specialElements=new Array();
						for (var i=0;i<response.data.length;i++){
							var toShowAsTerm=true;
							var term=response.data[i];
							
							for (var j=0;j<$scope.root.members.length;j++){
								var element=$scope.root.members[j];
								if (null !=element.relatedTerm && term.skosNotation==element.relatedTerm.skosNotation){
									if (term.codingSchemeOID=element.relatedTerm.codingSchemeOID){
										toShowAsTerm=false;
										$scope.specialElements.push(element);
									}
								}
							}
							if (toShowAsTerm==true)$scope.specialSubTerms.push(term);
						}
					}
					$scope.experimental=false;
			}
		);
	};
	
	
	// subTerm is an element inside the history array.
	$scope.showSubTerms= function (uriElement, uriRelated,subTerm) {
		$scope.hideView=false;
		$scope.zoomOnSubTerm=null;
		$scope.showedSubTerm=null;
		
		if (uriElement!=null){
			$scope.showed=uriElement;
			$scope.termHistory=null;
		}
		
		
		$scope.workInprogress=true;
		var paramsArray={uri: uriRelated };
		var url='rest/pivot/getTerm';
		
		restService.execute(paramsArray,url).then(function(response){
				
				$scope.zoomOn=null;
				$scope.termView=response.data;
				
				if (uriElement==null){
					if ($scope.termHistory==null){
						$scope.termHistory=new Array();
						$scope.termHistory[0]=response.data;
					}else {
						if (subTerm!=null){
							var i=$scope.termHistory.indexOf(subTerm);
							
							for (var j=i;j<$scope.termHistory.length;j=j+1){
								$scope.termHistory.splice(j);
							}
						}
						$scope.termHistory.push(response.data);
						
					}
				}
				$scope.workInprogress=false;
			});
	};
	
	
	
	// Here explore will find the related schemes.
	// Step 1 : find the List of Members.
	$scope.getRelatedSchemes = function (uriSelected){
		if (uriSelected==null)return;
		
		$scope.cleanupTheView();
		
		
		$scope.termHistory=null;
		$scope.hideView=false;
		$scope.showed=null;
		$scope.termView=null;
		$scope.workInprogress=true;
		
		var paramsArray={uri: uriSelected };
		var url='rest/context/element';
		restService.execute(paramsArray,url).then(function(response){
				
				$scope.root=response.data;
				$scope.zoomOn=null;
			
				console.log($scope.root);
				var relatedURISchemes=new Array();
				$scope.relatedSchemes=new Array();
				
				for(var i=0;i<$scope.root.members.length;i++){
					if ($scope.root.members[i].relatedTerm==null){
						// abiraterone bizarre .. 
					}
						else {
							if (relatedURISchemes.indexOf($scope.root.members[i].relatedTerm.ehr4crCodingSchemeURI)<0){
						
							relatedURISchemes.push($scope.root.members[i].relatedTerm.ehr4crCodingSchemeURI);
							var scheme={uri:$scope.root.members[i].relatedTerm.ehr4crCodingSchemeURI,
									    label:$scope.root.members[i].relatedTerm.codingSchemeName};
							$scope.relatedSchemes.push(scheme);
						}
					}
				}

				
				
				
				$scope.workInprogress=false;
			});
			
		
	};
	
	
	
	$scope.explore = function (uriSelected){
		
		
		
		if (uriSelected==null)return;
		
		$scope.cleanupTheView(); 
		
		
		$scope.termHistory=null;
		$scope.hideView=false;
		$scope.showed=null;
		$scope.termView=null;
		$scope.workInprogress=true;
		var paramsArray={uri: uriSelected };
		var url='rest/context/element';
			restService.execute(paramsArray,url).then(function(response){
				
				$scope.root=response.data;
				$scope.zoomOn=null;
				$scope.workInprogress=false;
			});
	};
	
} 

CentralCtrl2.$inject = ['$rootScope','$scope','restService'];

function TestCtrl($http, $rootScope,$scope,restService){
	$http.defaults.useXDomain=true;
	$rootScope.header='test';
	
}
TestCtrl.$inject = ['$http','$rootScope','$scope','restService'];



function CentralCtrl3($rootScope,$scope,restService){
	$rootScope.header='central3';
	$scope.zoomOn=null;
	$scope.workInprogress=true;
	$scope.showed=null;
	$scope.termView=null;
	$scope.termHistory=null;
	$scope.hideView=true;
	$scope.relatedSchemes=null;
	$scope.experimental=false;
	var paramsArray={ };
	
	$scope.cleanupTheView= function (){
		$scope.relatedSchemes=null;
		$scope.focusOnScheme=null;
		
		$scope.specialSubTerms=null;
		$scope.specialElements=null;
		$scope.specialTermHistory=null;
	};
	
	
	var url='rest/context';
		restService.execute(paramsArray,url).then(function(response){
			$scope.root=response.data;
			$scope.workInprogress=false;
		});
	
	$scope.showTerm = function(subTerm){
		$scope.hideView=true;
		$scope.zoomOnSubTerm=subTerm;
		$scope.showedSubTerm=subTerm.uri;
	};
	
	$scope.unShowTerm = function(subTerm){
		$scope.hideView=false;
		$scope.zoomOnSubTerm=null;
	}
	
	$scope.show = function (element){
		//$scope.termView=null;
		$scope.zoomOnSubTerm=null;
		$scope.showedSubTerm=null;
		$scope.hideView=true;
		$scope.zoomOn=element;
		if ($scope.showed!=null)$scope.tmp=$scope.showed;
		$scope.showed=element.uri;
		
	};
	
	$scope.tmp=null;
	
	$scope.unshow = function (element){
		$scope.hideView=false;
		$scope.zoomOn=null;
		$scope.showed=$scope.tmp;
	};
	
	$scope.unshowForLeaf = function (element){
		$scope.hideView=false;
		$scope.zoomOn=null;
		if ($scope.termView!=null)$scope.showed=element.uri;
		else $scope.showed=null;
	};
	
	$scope.showSpecialTopTerms= function (scheme){
		$scope.focusOnScheme=scheme;
		$scope.specialTermHistory=null;
		$scope.specialElements=null;
		
		$scope.specialSubTerms=null;
		$scope.termHistory=null;
		$scope.termView=null;
		$scope.showed=null;
		$scope.specialElements=null;
		
		$scope.specialSubTerms=new Array();
		$scope.specialElements=new Array();
		var topBroaders=new Array();
		var filter=new Array();
		for (var j=0;j<$scope.root.members.length;j++){
			var element=$scope.root.members[j];
			if (null !=element.relatedTerm){
				if (element.relatedTerm.topBroaders.length==0){
					if (element.relatedTerm.ehr4crCodingSchemeURI==scheme.uri){
						$scope.specialElements.push(element);
					}
				}
				for (var k=0;k<element.relatedTerm.topBroaders.length;k++){
					var candidate=element.relatedTerm.topBroaders[k];
					if (candidate.ehr4crCodingSchemeURI==scheme.uri){
						var toAdd=true;
						for (var l=0;l<filter.length;l++){
							if (filter[l]==candidate.skosNotation){
								// do nothing...
								toAdd=false;
							}
							
						}
						if (toAdd){
							filter.push(candidate.skosNotation);
							 topBroaders.push(candidate);
						}
						
					}
				}
			}
			
		 }
		$scope.specialSubTerms=topBroaders;
			
		
		
		//return $scope.showSpecialSubTerms(scheme,false);
	};
	
	
	
	
	
	$scope.showSpecialSubTermsComingFromHistory=function (element){
		
			var i=$scope.specialTermHistory.indexOf(element);
			
			for (var j=i;j<$scope.specialTermHistory.length;j=j+1){
				$scope.specialTermHistory.splice(j);
			}
		
			
		
		return $scope.showSpecialSubTerms(element,true);
	};
	
	
	
	$scope.showSpecialSubTerms=function (element,notAScheme){
		
		if (notAScheme==null)notAScheme=true;
		
		$scope.experimental=true;
		$scope.specialSubTerms=null;
		$scope.termHistory=null;
		$scope.termView=null;
		$scope.showed=null;
		$scope.specialElements=null;
		
		if (notAScheme==true){
			if ($scope.specialTermHistory==null){
				
				$scope.specialTermHistory=new Array();
			}
			$scope.specialTermHistory.push(element);
			console.log("SpecialTermhistory="+ $scope.specialTermHistory);
		}
		
		var paramsArray={elementUri: $scope.root.uri , schemeOrTermUri: element.uri   };
		var url='rest/context/validateSupTermsForMembers';
		restService.execute(paramsArray,url).then(function(response){
			
					
					if (response.data.info1 !=null){
						// do nothing ...
					}
					else {
						
						
						
						$scope.specialSubTerms=null;
						// $scope.relatedSchemes=null;
						$scope.specialSubTerms=new Array();
						$scope.specialElements=new Array();
						for (var i=0;i<response.data.length;i++){
							var toShowAsTerm=true;
							var term=response.data[i];
							
							for (var j=0;j<$scope.root.members.length;j++){
								var element=$scope.root.members[j];
								if (null !=element.relatedTerm && term.skosNotation==element.relatedTerm.skosNotation){
									if (term.codingSchemeOID=element.relatedTerm.codingSchemeOID){
										toShowAsTerm=false;
										$scope.specialElements.push(element);
									}
								}
							}
							if (toShowAsTerm==true)$scope.specialSubTerms.push(term);
						}
					}
					$scope.experimental=false;
			}
		);
	};
	
	
	// subTerm is an element inside the history array.
	$scope.showSubTerms= function (uriElement, uriRelated,subTerm) {
		$scope.hideView=false;
		$scope.zoomOnSubTerm=null;
		$scope.showedSubTerm=null;
		
		if (uriElement!=null){
			$scope.showed=uriElement;
			$scope.termHistory=null;
		}
		
		
		$scope.workInprogress=true;
		var paramsArray={uri: uriRelated };
		var url='rest/pivot/getTerm';
		
		restService.execute(paramsArray,url).then(function(response){
				
				$scope.zoomOn=null;
				$scope.termView=response.data;
				
				if (uriElement==null){
					if ($scope.termHistory==null){
						$scope.termHistory=new Array();
						$scope.termHistory[0]=response.data;
					}else {
						if (subTerm!=null){
							var i=$scope.termHistory.indexOf(subTerm);
							
							for (var j=i;j<$scope.termHistory.length;j=j+1){
								$scope.termHistory.splice(j);
							}
						}
						$scope.termHistory.push(response.data);
						
					}
				}
				$scope.workInprogress=false;
			});
	};
	
	
	
	// Here explore will find the related schemes.
	// Step 1 : find the List of Members.
	$scope.getRelatedSchemes = function (uriSelected){
		if (uriSelected==null)return;
		
		$scope.cleanupTheView();
		
		
		$scope.termHistory=null;
		$scope.hideView=false;
		$scope.showed=null;
		$scope.termView=null;
		$scope.workInprogress=true;
		
		var paramsArray={uri: uriSelected };
		var url='rest/context/element';
		restService.execute(paramsArray,url).then(function(response){
				
				$scope.root=response.data;
				$scope.zoomOn=null;
			
				console.log($scope.root);
				var relatedURISchemes=new Array();
				$scope.relatedSchemes=new Array();
				
				for(var i=0;i<$scope.root.members.length;i++){
					if ($scope.root.members[i].relatedTerm==null){
						// abiraterone bizarre .. 
					}
						else {
							if (relatedURISchemes.indexOf($scope.root.members[i].relatedTerm.ehr4crCodingSchemeURI)<0){
						
							relatedURISchemes.push($scope.root.members[i].relatedTerm.ehr4crCodingSchemeURI);
							var scheme={uri:$scope.root.members[i].relatedTerm.ehr4crCodingSchemeURI,
									    label:$scope.root.members[i].relatedTerm.codingSchemeName};
							$scope.relatedSchemes.push(scheme);
						}
					}
				}

				
				
				
				$scope.workInprogress=false;
			});
			
		
	};
	
	
	
	$scope.explore = function (uriSelected){
		
		
		
		if (uriSelected==null)return;
		
		$scope.cleanupTheView(); 
		
		
		$scope.termHistory=null;
		$scope.hideView=false;
		$scope.showed=null;
		$scope.termView=null;
		$scope.workInprogress=true;
		var paramsArray={uri: uriSelected };
		var url='rest/context/element';
			restService.execute(paramsArray,url).then(function(response){
				
				$scope.root=response.data;
				$scope.zoomOn=null;
				$scope.workInprogress=false;
			});
	};
	
} 

CentralCtrl3.$inject = ['$rootScope','$scope','restService'];
