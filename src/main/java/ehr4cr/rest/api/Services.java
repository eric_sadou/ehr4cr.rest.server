package ehr4cr.rest.api;


import java.io.File;
import java.util.List;
import java.util.Set;
 
import org.openrdf.model.URI;

import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.services.shared.ErsatzTerm;
import ubiqore.services.shared.Scheme;
import ubiqore.services.shared.SimpleTerm;
import ubiqore.services.shared.Term;
import ehr4cr.rest.context.data.Ehr4crElement;


  
/*
 * The services available definitions
 * 
 * http://server.ubiqore.com:9200/ehr4cr-centrale/vertex/_search?q=_all:blo*
 * http://server.ubiqore.com:9200/ehr4cr-centrale/vertex/_search?q=_all:271650006
 * 
 */

public interface Services {
	/**
	 Schemes Services (MultiTerminology Graph Access Services.) for WorkBench
	 */
	public Set<Scheme> getSchemes();
	public Term getTerm(String code,String oid); // Term with direct Narrow should be send.
	public Term getTerm(String termURI); // Term with direct Narrow should be send.
	public Term getTermAndLevelSubTerms(String termURI,int level); // Term with full ErsatzTerm List (subClasses)
	public Set<SimpleTerm> searchSubTerms(String key,String domainURI);
	public Set<ErsatzTerm> searchTerms(String pattern,String mode,String optionalSchemeUri,boolean auto);
	public Set<ErsatzTerm> getDirectSupTerms(String termURI);
	/**
	 * Schemes Services for Mapping endpoints ressources
	 */
	public File getCodes(char separ,String type,int sublevels);
	
	public File getRDF(String conceptUri);
	
	/**
	 * Context (DataElements) Services
	 */
	public Ehr4crElement getRoot();
	public Ehr4crElement getEhr4crElement(String uri);
	public List<Ehr4crElement> searchEhr4crElement(String str);
	public List<Ehr4crElement> searchEhr4crElementPro(String pattern,
													  String type,
													  boolean exactMatch,
													  boolean autocompletion,
													  URI metadata,
													  URI memberOfUri,
													  URI topCategoryUri,
													  URI schemeUri,
													  int maxres,
													  int offset
													  );
	// return a list of subterm of schemeOrTermUri with hierarchical relation to elementUri members.
	public Set<ErsatzTerm> validateSupTermsForMembers(String elementUri,String schemeOrTermUri );
	
	 /* 
	Mapping Service To keep for Now !! 
	 */
	public ErsatzConcept getErsatzConcept(String key,String value, String codingSchemeURI);
	
	/*
	 * Mapping Service [new services April 2013]
	 * 
	 */
	public Set<Term> validate (Set<Term> set);
	
	
	public void disconnect();
	public void reconnect();
	public String state();
	
	/*
	 *
	
	
	
	public List<ErsatzConcept> searchSubTerms(String key,String domainURI);
	public List<ErsatzConcept> getSubTerms(String domainURI);
	public File exportXMLRDF();
	public String disconnect(String adminPassword);
	public String connect(String adminPassword);

	public Ehr4crElement getDataElement(EntityNameOrUri uri);*/
	
	/*public final static String ALL="all";
	*//**
	 *  key : must be a unique index key (urn, external_id, DESCRIPTION ) OPTIONAL 
	 *  for information (_all search by default --> check all indices)
	 *  value : the value (complete no text vocabulary) --> not a search . REQUIREMENT 
	 *  RETURN : a concept definition without links. (See the ErsatzConcept definition please.)
	 *//*
	public ErsatzConcept getErsatzConcept(String key,String value, String codingSchemeURI);
	
	
	*//**
	 *  key : must be a unique index key (urn, external_id, DESCRIPTION ) OPTIONAL 
	 *  for information (_all search by default --> check all indices)
	 *  value : the value (complete no text vocabulary) --> not a search . REQUIREMENT 
	 *  RETURN : a concept definition with his direct environment sets (first level ISA fathers(Bag of ErsatzConcepts) , First level ISA sons (Bag of ErsatzConcept) 
	 *  The Bag of the concept are never null , but can be empty.
	 *//*
	public Concept getConcept(String key,String value, String codingSchemeURI);
	
	*//**
	 * 
	 * @return a bag of ErsatzConcept , branch of the pivot terminology.
	 *//*
	public Set<ErsatzConcept> getBranches(String codingSchemeURI); 
	
	*//**
	 * @search a string elasticSearch vocabulary. 4 characteres minimum.
	 * @return the best result object.
	 *//*
	public Concept luckySearch (String search,String codingSchemeURI);
	
	*//**
	 * @search a string elasticSearch vocabulary. 4 characteres minimum.
	 * @return all the corresponding concept (As a ersatzConcept). return is a list (result are classified as best first in the list..)
	 *//*
	public List<ErsatzConcept> searchConcept(String search,String codingSchemeURI);
	
	public QuerySearchAnswer searchConceptWithLimit(String search,String codingSchemeURI,int limit,int from);
	
	public List<ErsatzConcept> searchConceptByKey(String key,String value,String codingSchemeURI);
	
	
	public boolean linkConceptToValueSet(String conceptUrn,String valueSetUrn,boolean replace);
	
	public boolean linkConceptToUnit(String conceptUrn,String unitUrn,boolean replace);
	public Set<SimpleConcept> getSubConceptsList(String key, String value,String codingSchemeURI,int level);
	
	*/
}
