package ehr4cr.rest.api;
  
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
  
import org.joda.time.DateTime;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.sail.SailException;

import ubiqore.graphs.semantic.ContextElementsGraph;
import ubiqore.graphs.semantic.ContextVocabulary;
import ubiqore.graphs.semantic.DataElement;
import ubiqore.graphs.semantic.SemanticVocabulary;
import ubiqore.graphs.semantic.SkosConcept;
import ubiqore.graphs.semantic.TripleAndContextGraph;
import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.patterns.concept.Property;
import ubiqore.services.shared.ErsatzTerm;
import ubiqore.services.shared.Scheme;
import ubiqore.services.shared.SimpleTerm;
import ubiqore.services.shared.Term;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import ehr4cr.rest.context.data.Ehr4crElement;
import ehr4cr.rest.definitions.MappingTools;

public class ServicesImpl implements Services{
	
	String tmp="/opt/erics/tmp/";
	
	public static void main(String ... args){
		ServicesImpl s=new ServicesImpl("/Users/roky/Reborn/data/ehr4cr/graphs/fev/context","/Users/roky/Reborn/data/ehr4cr/graphs/fev/full");
		
		/*PrintStream console = System.out;
		FileOutputStream fos=null;
		try {
			fos = new FileOutputStream(new File ("/Reborn/tmp/console.log"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PrintStream ps = new PrintStream(fos);*/
		//System.setOut(ps);
		
		try {
			s.context.exportRDF("/Users/roky/Reborn/toto.rdf");
			Ehr4crElement el=s.getRoot();
			
		    System.out.println(el.getLabel());
			
			 if (true)throw new Exception();
			//System.out.println("After"+ressources.searchLabel("alert",new URIImpl( "http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.1"), 10, false));
			
			/*System.out.println("After"+ressources.searchCode("C70", new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.3"), 10,true));
			if (true)throw new Exception();*/
		  // if (true)throw new Exception();
			/* Drug/Tox   */
			//System.out.println("Before"+ressources.searchByLabel2("Female", null, 1));
			System.out.println("AfterOne"+ressources.searchCode("399204005",null, 1,0,true));
			System.out.println("START OF THE REAL TESTSSSSSSS");
			//System.out.println("AfterTwo"+  ressources.searchCodeFrom("36",new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.1"), 10,0,true));
			System.out.println("AfterTwo"+  ressources.searchCode("36",new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.1"), 10,0,true));
			 if (true)throw new Exception();
			System.out.println("AfterThree"+ressources.searchCode("xxxxxxxxxxxxx",new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.1"), 1,0,false));
			System.out.println("AfterThree2"+ressources.searchCode("xxxxxxxxxxxxx",new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.1"), 1000000,0,false));
			//System.out.println("Before"+ressources.searchLabel("Female", new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.1"), 1,0,true));
			 if (true)throw new Exception();
			System.out.println("Before"+ressources.searchLabel("imaging", new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.1"), 5,5,false));
			if (true)throw new Exception();
			System.out.println("Before"+ressources.searchLabel("multisection^W", null, 10,0,true));
			System.out.println("After"+ressources.searchCode("c70",new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.3"), 10,0,false));
			System.out.println("After"+ressources.searchCode("c70",null, 10,0,false));
			
			if (true)throw new Exception();
			
			//	System.out.println("Before"+ressources.searchLabel("Breast-Infiltrating malignant neoplasm-HER2", null, 10,false).size());
			
			
			System.out.println("top");
			if (true)throw new Exception();
			//System.out.println("Before"+ressources.searchCode("C70", new URIImpl("http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.3"), 10,false).size());
			if (true)throw new Exception();
			
			SkosConcept skosC=ressources.getConcept(new URIImpl("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.1/LP32651-9"));
			
			ressources.exportRDFOfSubTree("/Users/roky/Desktop/test1.rdf", skosC.getUri(), 1);
			ressources.exportRDFOfSubTree("/Users/roky/Desktop/test4.rdf", skosC.getUri(), 4);
			ressources.exportRDFOfSubTree("/Users/roky/Desktop/test0.rdf", skosC.getUri(), 0);
			System.out.println(skosC.getSkosNotation());
			System.out.println("start of treatment");
			Set<URI> searchByLabelSets=s.ressources.searchLabel("erith", null, 10000,0,true);
			for (URI uri:searchByLabelSets){
				System.out.println("2"+uri.stringValue());
			}
			
			if (true)throw new Exception();
			
			
			searchByLabelSets=s.ressources.searchCode("Q00", null, 10,0,true);
			for (URI uri:searchByLabelSets){
				System.out.println("2"+uri.stringValue());
			}
			
			if (true)throw new Exception();
			s.ressources.statements(new URIImpl("http://server.ubiqore.com/ehr4cr/rest/pivot/2.16.840.1.113883.6.96/123037004"), false);
			
			Set<ErsatzTerm> set=s.validateSupTermsForMembers("http://server.ubiqore.com/v1/DataElement/EHR4CR_VS_1.2", "http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.96");
			if (set==null)throw new Exception();
			else {
				for (ErsatzTerm t:set){
					System.out.println("candidat OK="+t.getUri());
				}
			}
			 
			Set<ErsatzTerm> set2=s.validateSupTermsForMembers("http://server.ubiqore.com/v1/DataElement/EHR4CR_VS_9", "http://server.ubiqore.com/v1/scheme/2.16.840.1.113883.6.3");
			if (set2==null)throw new Exception();
			else {
				for (ErsatzTerm t:set2){
					System.out.println("candidat OK="+t.getUri());
				}
			}
			System.out.println("end of Treatment");
		//s.context.statements();
		// s.getRoot(); // OK
		
		// System.out.println(s.getSchemes().toString()); OK
		// s.getCodes('$', DEVoc.FULL, 0); OK
		//System.out.println(s.getTerm("S00-T98.9", "2.16.840.1.113883.6.3").toString()); OK
		//System.out.println(	s.getTerm("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.3/T66-T78.9").toString()); OK
		//System.out.println(s.getEhr4crElement("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.73/B01AA03/CD").toString()); 
			
		//System.out.println(s.getEhr4crElement("http://server.ubiqore.com/v1/DataElement/EHR4CR_VS_9").toString()); 
		//http://server.ubiqore.com/v1/DataElement/EHR4CR_VS_9 KO
		//s.context.statements(new URIImpl("http://server.ubiqore.com/v1/DataElement/EHR4CR_VS_1.6"), true);
		//System.out.println(s.ressources.getNarrowersBest(4).size());
			
		//Term cinq=s.getTermAndLevelSubTerms("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.3/T66-T78.9", 5); OK
		//Term trois=s.getTermAndLevelSubTerms("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.3/T66-T78.9", 3); OK
		//System.out.println("cinq-->"+cinq.getNbSubClasses()); OK 
		//System.out.println("trois-->"+trois.getNbSubClasses()); OK
		
		/*Date d1=new Date();
		System.out.println(s.ressources.searchInDomainNewTest("hiv", new URIImpl("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.3/B20-B24.9")).toString());
		Date d2=new Date();
		System.out.println((d2.getTime()-d1.getTime())/1000+" s");*/
		/*Date d3=new Date();
		//System.out.println(s.ressources.getOriginalList().size());
		System.out.println(s.ressources.getConcept(new URIImpl("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.3/B20.2")).toString());
		System.out.println(s.ressources.searchInDomain("hiv", new URIImpl("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.3/B20-B24.9")).size());
		Set<String> sets=s.ressources.getBagOfNarrowers( "http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.3/B20-B24.9", 3);
		for (String code:sets){
			System.out.println(s.ressources.getConcept(new URIImpl(code)).toString());
		}
		Date d4=new Date();
		System.out.println((d4.getTime()-d3.getTime())/1000+" s");*/
		//System.out.println("ECOG search ="+s.searchEhr4crElement("ECOG").size()); 
		////System.out.println("eco search ="+s.searchEhr4crElement("eco").size());   OK
		
		
		
		
		}catch(Exception e ){e.printStackTrace();}
		finally {
			s.disconnect("neo4j");
			//System.setOut(console);
		}
	}
	public static ContextElementsGraph context=null;
    public static TripleAndContextGraph ressources=null;
    
    public void disconnect() {
    	try {
    		context.disconnect();
    		context=null;
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	try {
    		ressources.disconnect();
    		ressources=null;
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
    public void reconnect(){
    	this.init();
    }
	public String state(){
		if (context!=null)return "running";
		else return "down";
	}
    
	public ServicesImpl(String pathContext,String pathResources){
		this.init(pathContext, pathResources);
	}
	
	private void init(){
		this.init(null,null);
	}
	
	public void init(String pathContext,String pathResources) {
		
	    	if (context==null){
	    		System.out.println("start of contextual init for ehr4cr server");
	    		
	    		
	    		
	    		// "/Reborn/data/ehr4cr/graphs/june/context"
	    		System.out.println("context graph path="+pathContext);
	    		try {
	    			context=new ContextElementsGraph(false,pathContext);
	    		// LOCAL	context=new ContextElementsGraph(false,"/Reborn/data/ehr4cr/graphs/mars2013/context");
	    		/* PROD	context=new ContextElementsGraph(false,"/Reborn/data/ehr4cr/graphs/test/context");
				*/} catch (SailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	if (ressources==null){
	    		//String pathResources= sc.getInitParameter("resources");
    			System.out.println("context graph path="+pathResources);
	    		try {
	    			
	    			// "/Reborn/data/ehr4cr/graphs/june/full" 
	    			ressources= new TripleAndContextGraph(false,pathResources);
	    			
				//	ressources= new TerminologiesGraph(false,"/Reborn/data/ehr4cr/graphs/mars2013/resources");
	    			//ressources= new TerminologiesGraph(false,"/Reborn/data/ehr4cr/graphs/test/full");
				} catch (SailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		System.out.println(" contextual init is ok");
	    	}
	    }
	
	@Override
	protected void finalize() throws Throwable {
		this.beforeEnd();
		System.out.println("The services have been closed");
		super.finalize();
	}
	
	private  String show(String filePath,String dataTypeName,Set<String> set){
		File file=new File(filePath);
		
		PrintStream console = System.out;

		
		FileOutputStream fos=null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PrintStream ps = new PrintStream(fos);
		System.setOut(ps);
		//System.out.println("This goes to out.txt");

		
		Iterator<String> ite=set.iterator();
		List<String> list=Lists.newArrayList();
		
		while (ite.hasNext()){
			list.add(ite.next());
			// System.out.println(ite.next());
		}
		Collections.sort(list,String.CASE_INSENSITIVE_ORDER);
		for (String st:list){
			System.out.println(st);
		}
		
		System.setOut(console);
		
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss") ;
		String zipName=dateFormat.format(date)+".zip";
		try {
			this.zipFile(filePath,dataTypeName+".tsv",tmp+zipName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tmp+zipName;
	}
	private void zipFile(String filePath,String fileName,String zipPath) throws IOException{
		
		FileInputStream in = new FileInputStream(filePath);
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipPath));
        // name the file inside the zip  file 

        try {
			out.putNextEntry(new ZipEntry(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

           byte[] b = new byte[1024];

       int count;

       while ((count = in.read(b)) > 0) {
           System.out.println();

        out.write(b, 0, count);
       }
       out.close();
       in.close();
       }

	@Override
	public File getRDF(String conceptUri){
		String where=tmp+new DateTime().getMillis()+".rdf";
		 try {
			ressources.exportRDFOfSubTree(where,new URIImpl( conceptUri), 0);
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 if (new File(where).exists())
		 return new File(where);
		 else return null;
	}
	
	@Override
	public File getCodes(char separ, String type, int sublevels) {
		
		Set<String> set=Sets.newHashSet();
		
		String dataTypeName="data";
		
		List<SkosConcept> TheList=Lists.newArrayList();
		
		try {
			// Update October : UCUM ds l avant !!
			Set<String> ucums=context.getUCUMCodeList();
			for (String code:ucums){
				try {
					SkosConcept aUcum=ressources.getConcept(ressources.getConceptUri(code,"2.16.840.1.113883.6.8"));
					if(aUcum==null)System.err.println("issue 1 with code="+code);
					else {
						aUcum.setPeriod("October:2013");
						TheList.add(aUcum);
					}
				}catch (Exception e ){
					System.err.println("issue 2 with code="+code);
				}
			}
			
			if (type.equalsIgnoreCase(ContextVocabulary.EXPANDED)){
				dataTypeName=ContextVocabulary.EXPANDED;
				Set<URI> uris=ressources.getConceptsURI();
				for (URI uri:uris)TheList.add(ressources.getConcept(uri));
			}
			else if (type.equalsIgnoreCase(ContextVocabulary.FULL ) ||
				(type.equalsIgnoreCase(ContextVocabulary.LEVELS)&& sublevels>0)
			   ){
				if (type.equalsIgnoreCase(ContextVocabulary.FULL )){
					sublevels=0; // 0 means ALL the sublevels for this service.
					dataTypeName=ContextVocabulary.FULL;
				}else {
					dataTypeName=ContextVocabulary.LEVELS;
				}
				
				Set<String> s=ressources.getSubOriginFromLevels(sublevels);
				for (String st:s)TheList.add(ressources.getConcept(new URIImpl(st)));
			}else { // LEVELS + 0 means only single list.
				TheList.addAll(ressources.getOriginalList());
				dataTypeName="original";
			}
			
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			
			e.printStackTrace();
		}
		for (SkosConcept s:TheList){
			this.addToSet(separ,s,set);
		}
		
		System.out.println("nbLines to write="+set.size());
		
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss") ;
		
		
		String fileName=tmp+dateFormat.format(date);
		
		File zip=new File(this.show(fileName,dataTypeName,set));
		return zip;
	}
	
	// ici on doit ajouter la date !! 
	
	private void addToSet(char separ,SkosConcept s, Set<String>  set){
		String period=separ+"-";
		if (s.getPeriod()!=null && !s.getPeriod().isEmpty())period=separ+s.getPeriod();
		
		set.add("urn:oid:"+s.getScheme().getOid()+separ+s.getScheme().getDef()+separ+s.getSkosNotation()+separ+s.getPreflabel().getLit().getLabel()+period);
		
		if (s.getCoValue()!=null){
			set.add("urn:oid:"+s.getScheme().getOid()+separ+s.getScheme().getDef()+separ+s.getSkosNotation()+"/"+s.getCoValue().toString()+separ+s.getPreflabel().getLit().getLabel()+period);
		}
	}
	
	/*
	 * Data Elements Services (Context)
	 */
	@Override
	public Ehr4crElement getRoot() {
		// TODO Auto-generated method stub
		try {
			DataElement e=context.getRoot();
			return this.generate(e,null);
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	private Set<ErsatzTerm> getGoodTopFrom(String elementUri,String schemeUri){
		
		Ehr4crElement element=this.getEhr4crElement(elementUri);
		if (element.getMembers()==null|| element.getMembers().isEmpty())return null;
		Set<ErsatzTerm> set=Sets.newHashSet();
		Set<String> goods=Sets.newHashSet();
		
		
		Set<String> topURIs=Sets.newHashSet();
		try {
			Set<URI> topURIsTmp = ressources.getTopConceptsOfScheme(new URIImpl(schemeUri));
			for (URI uri:topURIsTmp){
				topURIs.add(uri.stringValue());
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String toMove=null;
		for (Ehr4crElement  preE:element.getMembers()){
			boolean isTreated=false;
			
			if (toMove!=null){
				topURIs.remove(toMove);
			}
			
			
			if (preE.getRelatedTerm()!=null){
				 if (! preE.getRelatedTerm().getEhr4crCodingSchemeURI().equalsIgnoreCase(schemeUri) ){
					 isTreated=true;
				 }
				try {
					if (!isTreated){
						for (String uri:topURIs){
							if (uri.equalsIgnoreCase(preE.getRelatedTerm().getUri())){
								goods.add(uri);
								toMove=uri;
								isTreated=true;
							}
						}
					}
					if (!isTreated){
						Set<String> tops=ressources.getTopTerms(preE.getRelatedTerm().getUri(), schemeUri);
						goods.addAll(tops);
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if (!goods.isEmpty()){
			for (String good:goods){
				try {
					set.add(this.getErsatzTerm(good));
				} catch (SailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MalformedQueryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (QueryEvaluationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return set;
	}
	
	// return a list of subterm of schemeOrTermUri with hierarchical relation to elementUri members.
	@Override
	public Set<ErsatzTerm> validateSupTermsForMembers(String elementUri,String schemeOrTermUri ){
			init();
			try {
				ubiqore.graphs.semantic.Scheme schemeTmp=ressources.getSchemeBase(new URIImpl(schemeOrTermUri));
				
				if (schemeTmp!=null){
					return this.getGoodTopFrom(elementUri,schemeOrTermUri);
				}
			} catch (SailException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			} catch (MalformedQueryException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			} catch (QueryEvaluationException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			 Set<ErsatzTerm> valids=Sets.newHashSet();
			 Set<String> validTemSet=Sets.newHashSet();
			 
			 Set<String> candidates=Sets.newHashSet();
			Ehr4crElement element=this.getEhr4crElement(elementUri);
			if (element.getMembers()==null|| element.getMembers().isEmpty())return null;
			
			 Set<URI> topURI=null;
			try {
				topURI = ressources.getTopConceptsOfScheme(new URIImpl(schemeOrTermUri));
				
			} catch (SailException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (MalformedQueryException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (QueryEvaluationException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			 
			 if (topURI !=null && !topURI.isEmpty()){
				 System.out.println(topURI.size() + "NB Top Elements ");
				 for (URI u:topURI){
				 candidates.add(u.stringValue());
				 }
				}else {
					ErsatzTerm term=null;
					Set<String> uris=null;
					try {
						term = this.getErsatzTerm(schemeOrTermUri);
						uris=this.ressources.getBagOfNarrowers(term.getUri(),1);
					} catch (SailException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (MalformedQueryException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (QueryEvaluationException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					if (uris==null||uris.isEmpty())return null;
					else candidates=uris;
				}
			 
			
			
			ArrayList<Ehr4crElement> tocheck=Lists.newArrayList();
			
			// recherche des egalités 
			for (Ehr4crElement  preE:element.getMembers()){
				if (preE.getRelatedTerm()!=null){
					if (candidates.contains(preE.getRelatedTerm().getUri())){
						
						    validTemSet.add(preE.getRelatedTerm().getUri());
							//valids.add(preE.getRelatedTerm());
							candidates.remove(preE.getRelatedTerm().getUri());
					}
					else {
							tocheck.add(preE);
					}
				}
			}
			
			// ici les egalités sont ds validTemSet !
			
			String toRemove=null;
			
			for (Ehr4crElement  e:tocheck){ // elements non egaux aux candidats hierarchiques...
				
				
				ErsatzTerm term=e.getRelatedTerm();
				
				//System.out.println("check of relation with "+term.getPreflabel());
				if (toRemove!=null){
					candidates.remove(toRemove);
					toRemove=null;
				}
				
				for (String domain:candidates){
					
					try {
							boolean tokeep=ressources.searchInDomain(new URIImpl(term.getUri()),new URIImpl(domain));
							if (tokeep){
								validTemSet.add(domain);
								//valids.add(this.getErsatzTerm(domain));
								toRemove=domain;
							}
						} catch (SailException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (MalformedQueryException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (QueryEvaluationException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					
					
				}
				
			}
			
			for (String valid:validTemSet){
				try {
					valids.add(this.getErsatzTerm(valid));
				} catch (SailException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (MalformedQueryException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (QueryEvaluationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			return valids;
	}
	
	@Override 
	public List<Ehr4crElement> searchEhr4crElementPro(String pattern,
			  String type,
			  boolean exactMatch,
			  boolean autocompletion,
			  URI metadata,
			  URI memberOfUri,
			  URI topCategoryUri,
			  URI schemeUri,
			  int maxres,
			  int offset
			  ){
		init();
		List<Ehr4crElement> l=Lists.newArrayList();
		try {
			String searchingType="labelPattern"; 
			
			String labelPattern=null;
			String termlabelPattern=null;
			String termCodePattern=null;
			String labelExactMatch=null;
			String termlabelExactMatch=null;
			String termCodeExactMatch=null;
			
			if (!Strings.isNullOrEmpty(type)){
				searchingType=type;
			}
			if (searchingType.equalsIgnoreCase("labelPattern")){
				labelPattern=pattern;
			}
			else if  (searchingType.equalsIgnoreCase("termlabelPattern")){
				termlabelPattern=pattern;
			}
			else if  (searchingType.equalsIgnoreCase("termCodePattern")){
				termCodePattern=pattern;
			}
			else if  (searchingType.equalsIgnoreCase("labelExactMatch")){
				labelExactMatch=pattern;
			}
			else if  (searchingType.equalsIgnoreCase("termlabelExactMatch")){
				termlabelExactMatch=pattern;
			}
			else if  (searchingType.equalsIgnoreCase("termCodeExactMatch")){
				termCodeExactMatch=pattern;
			}
			
			Set<DataElement> res=context.searchDataElement(metadata, 
															labelPattern, 
															termlabelPattern, 
															termCodePattern, 
															labelExactMatch, 
															termlabelExactMatch, 
															termCodeExactMatch, 
															schemeUri, 
															memberOfUri, 
															topCategoryUri, 
															maxres, 
															offset, 
															autocompletion);
			if (res==null)return l;
			if (res.isEmpty())return l;
			System.err.println("Nb dataElements good=>"+res.size());
			Iterator<DataElement> ite=res.iterator();
			while (ite.hasNext()){
				DataElement de=ite.next();
				l.add(this.generate(de, null));
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}
	
	@Override
	public List<Ehr4crElement> searchEhr4crElement(String str) {
		init();
		List<Ehr4crElement> l=Lists.newArrayList();
		try {
			Map<URI, String> uris=context.searchStringREGEX(str);
			if (uris==null)return l;
			if (uris.isEmpty())return l;
			Iterator<Entry<URI, String>> ite=uris.entrySet().iterator();
			while (ite.hasNext()){
				Entry<URI, String> ent=ite.next();
				l.add(this.getEhr4crElement(ent.getKey().stringValue()));
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}
	
	
	@Override
	public Ehr4crElement getEhr4crElement(String uri){
		try {
			DataElement de= context.getDataElementByURI(new URIImpl(uri),true);
			if (de==null)return null;
			SkosConcept concept =null;
			if (de.getRelatedCD()!=null){
				
				concept =ressources.getConcept(new URIImpl(de.getRelatedCD()));
				//concept.setBroaders(broaders);
			}
			return this.generate(de, concept);
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	 private Ehr4crElement generate(DataElement vse,SkosConcept concept){
		System.err.println(vse.getLabel());
		
		Ehr4crElement el=new Ehr4crElement();
		el.setLabel(vse.getLabel());
		el.setMemberOfUri(vse.getMemberOfUri());
		el.setTopCategoryUri(vse.getTopCategoryURI());
		if (vse.getMembers()!=null){
			/*if (vse.getMetadata().equalsIgnoreCase(DEVoc.DE_VS_String)){
				 Les membres sont des CD directement !!! 
				for (DataElement cd:vse.getMembers()){
					Ehr4crElement e=new Ehr4crElement();
					e.setLabel(cd.getLabel());
					e.setMemberOfUri(cd.getMemberOfUri());
					e.setMetadata(cd.getMetadata());
					e.setMembers(null);
					e.setRelatedTerm(ressources.getConcept(new URIImpl(cd.getUri())));
				}
			}
			else {*/
					for (DataElement member: vse.getMembers()){
						try {
						//System.out.println("member label "+member.getLabel());
						el.addMember(this.generate(member, ressources.getConcept(new URIImpl(member.getRelatedCD()))));
						} catch (SailException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (MalformedQueryException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (QueryEvaluationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				//}
			}
		}
		el.setMetadata(vse.getMetadata());
		el.setValue(vse.getCoValue());
		
		if (vse.getPqUcumCode()!=null){
			if (!vse.getPqUcumCode().trim().isEmpty()){
				Term unit=new Term();
				unit.setCodingSchemeName("ucum");
				unit.setCodingSchemeOID("2.16.840.1.113883.6.8");
				unit.setEhr4crCodingSchemeURI(null); // not inside terminology for now ..
				unit.setSkosNotation(vse.getPqUcumCode());
				unit.setPreflabel(vse.getPqUcumName());
				
				el.setUnit(unit);
			}
		}
		
		el.setUri(vse.getUri());
		if (concept!=null){
			
			Set<URI> topBroaders=Sets.newHashSet();
			try {
				topBroaders = context.getTopBroaders(concept.getUri());
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Set<SkosConcept> broaders=Sets.newHashSet();
				for (URI uriTB:topBroaders){
					SkosConcept aTopBroader=null;
					try {
						aTopBroader = ressources.getConcept(uriTB);
					} catch (SailException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (MalformedQueryException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (QueryEvaluationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					broaders.add(aTopBroader);
			}
				concept.setBroaders(broaders);
				el.setRelatedTerm(MappingTools.fillErsatzTerm(concept));	
		}
		return el;
	}
	
	@Override
	public Set<Scheme> getSchemes() {
		Set<Scheme> set=Sets.newHashSet();
		try {
			Set<URI> uris=ressources.getSchemesURI();
			for (URI uri:uris){
				
				 Scheme scheme=MappingTools.fillSchemeObject(ressources.getSchemeBase(uri));
				 Set<URI> topURI=ressources.getTopConceptsOfScheme(uri);
				 Set<ErsatzTerm> eTerms=Sets.newHashSet();
				 for (URI term:topURI){
					 eTerms.add(MappingTools.fillErsatzTerm(ressources.getConcept(term)));
				 }
				 scheme.setTopTerms(eTerms); 
				 set.add(scheme);
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return set;
	}

	private ErsatzTerm getErsatzTerm(String uri ) throws SailException, MalformedQueryException, QueryEvaluationException{
		return MappingTools.fillErsatzTerm(ressources.getConcept(new URIImpl(uri)));
	}
	
	@Override
	public Term getTerm(String code,String oid){
		
		// return this.getTerm(ContextVocabulary.context_v1+SemanticVocabulary.concept+oid+"/"+code);
		try {
			return this.getTerm(ressources.getConceptUri(code, oid).stringValue());
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public Term getTerm(String termURI) {
		return this.getTermAndLevelSubTerms(termURI,1);
	}
	@Override
	public Set<ErsatzTerm> getDirectSupTerms(String termURI){
		Set<ErsatzTerm> set=Sets.newHashSet();
		Set<String> uris=Sets.newHashSet();
		try {
			uris = ressources.getBroaders(termURI);
			if (uris.isEmpty()){
				return set; //empty 
			}
			else {
				for (String uri:uris){
					ErsatzTerm eTerm = MappingTools.fillErsatzTerm(ressources.getConcept(new URIImpl(uri)));
					set.add(eTerm);
				}
			}
			
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return set;
	}
	
	@Override
	public Term getTermAndLevelSubTerms(String termURI,int level) {
		// TODO Auto-generated method stub
		ErsatzTerm eTerm=null;
		Term term=null;
		try {
			eTerm = MappingTools.fillErsatzTerm(ressources.getConcept(new URIImpl(termURI)));
			if (eTerm==null)return null;
			else term=new Term();
			term.copy(eTerm); // ajout des fields tronc commun...
			Set<String> bagOfUris=ressources.getBagOfNarrowers(term.getUri(), level);
			Set<ErsatzTerm>  ets=Sets.newHashSet();
			for (String uri:bagOfUris)ets.add(this.getErsatzTerm(uri));
			term.setNarrowers(ets);
			if (level==0){
				term.setManagedSubLevels("ALL");
			}else {
			term.setManagedSubLevels(""+level);
			}
			if (level==1){
				term.setSemanticOfNarrowers(Term.DIRECT);
			}
			else term.setSemanticOfNarrowers(Term.TRANSITIVE);
			
			return term;
			
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	@Override
	public Set<ErsatzTerm> searchTerms(String pattern,String mode,String optionalSchemeUri,boolean auto){
		Set<ErsatzTerm> res=Sets.newHashSet();
		URI schemeURI=null;
		try {
			if (optionalSchemeUri!=null){
				if (!optionalSchemeUri.isEmpty())
					schemeURI=new URIImpl(optionalSchemeUri);
			}  
			Set<URI> set=Sets.newHashSet();
			if (null !=mode  && mode.equalsIgnoreCase("label"))
				set=ressources.searchLabel(pattern, schemeURI, 10,0,auto);
			else set=ressources.searchCode(pattern, schemeURI, 10,0,auto);
			if (null==set || set.isEmpty())return null;
			else {
				for (URI uri: set){
					res.add(this.getErsatzTerm(uri.stringValue()));
				}
			}
			
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}
	
	@Override
	public Set<SimpleTerm> searchSubTerms(String key, String domainURI) {
		Set<SimpleTerm> results=Sets.newHashSet();
		try {
			Map<URI, String> res=ressources.searchInDomain(key, new URIImpl(domainURI));
			if (res.isEmpty())return results;
			else {
				
				Iterator<Entry<URI,String>> ite=res.entrySet().iterator();
				while (ite.hasNext()){
					Entry<URI,String> entry=ite.next();
					SimpleTerm st=new SimpleTerm();
					st.setLabel(entry.getValue());
					st.setUri(entry.getKey().stringValue());
					results.add(st);
				}
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	 
	 protected void beforeEnd(){
	    	System.out.println("Destroy the Services !!!!!!!!!!!!!!!");
	    	this.disconnect("neo4j");
	    }
	 
	 protected String disconnect(String pass) {
			if (!pass.equalsIgnoreCase("neo4j"))return "wrong password";
			init();
			ressources.disconnect();
			context.disconnect();
			
		//	ressources.disconnect();
			return "disconnect";
		}
	

    // getErsatZconcept with analyse of the value with "/" ...
	 public ErsatzConcept getErsatzConcept(String key, String value,
				String codingSchemeURI) {
			init();
			ErsatzConcept ec=new ErsatzConcept();
			SkosConcept concept=ressources.getErsatzConcept(value,codingSchemeURI,true);
			
			ec.setCodingSchemeURI(codingSchemeURI); // on renvoie le meme codeSchemeURI transmis
			ArrayList<Property> a=Lists.newArrayList();
			Property p=new Property();
			p.setName("codingSchemeName");
			p.setValue(concept.getScheme().getPreflabel().getLit().getLabel());
			
			if (concept.getCoValue()!=null){
				Property v=new Property();
				v.setName("coded_ordinal_value");
				v.setValue(concept.getCoValue().toString());
				a.add(v);
			}
			
			a.add(p);
			ec.setProperties(a);
			// evolution possible : tester codingSchemeURI et toujours renvoyer sous la form urn:oid ...
			ec.setId(concept.getSkosNotation());
			ec.setLabel(concept.getPreflabel().getLit().getLabel());
			ec.setUrn(concept.getUri().stringValue());
			return ec;
		}
	@Override
	public Set<Term> validate(Set<Term> set) {
		Set<Term> rep=Sets.newHashSet();
		Set<SkosConcept> skoss=Sets.newHashSet();
		for (Term t:set){
			SkosConcept skos=MappingTools.fillSkos(t);
			skoss.add(skos);
		}
		try {
			Set<SkosConcept> skosrep= ressources.validation(skoss);
			for (SkosConcept skos:skosrep){
				Term t=MappingTools.fillTerm(skos);
				rep.add(t);
			}
			return rep;
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
}
