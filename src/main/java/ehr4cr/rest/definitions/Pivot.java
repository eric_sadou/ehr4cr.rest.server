package ehr4cr.rest.definitions;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.services.shared.ErsatzTerm;
import ubiqore.services.shared.Scheme;
import ubiqore.services.shared.SimpleTerm;
import ubiqore.services.shared.Term;
import ehr4cr.rest.api.Services;

/*
 * default : All the Managed Schemes inside the Pivot Terminology
 */

@Path("/pivot")
public class Pivot {
	
	// Services are shared By the Parameters Class
	public static Services services=null;
		
		
		
		@GET
		@Produces("application/json")
		public Response getSchemes() {
			Set<Scheme> categoryElement = services.getSchemes();
			if (categoryElement == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(categoryElement);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		public Response getSchemesPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/xml")
		@Produces("application/xml")
		public Response getSchemesXml() {
			Set<Scheme> categoryElement = services.getSchemes();
			GenericEntity<Set<Scheme>>  ge=new GenericEntity<Set<Scheme>>(categoryElement){};
			if (categoryElement == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(ge);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/xml")
		public Response getSchemesXmlPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/{oid}/{code}")
		@Produces("application/json")
		public Response getTerm(@PathParam("oid") String oid,@PathParam("code") String code) {
			Term term = null;
			try {term=services.getTerm(code, oid);}catch (Exception e){}
			if (term == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(term);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/{oid}/{code}")
		public Response getTermPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/xml/{oid}/{code}")
		@Produces("application/xml")
		public Response getTermXml(@PathParam("oid") String oid,@PathParam("code") String code) {
			Term term = null;
			try {term=services.getTerm(code, oid);}catch (Exception e){}
			if (term == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(term);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/xml/{oid}/{code}")
		public Response getTermXmlPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/xml/getTerm")
		@Produces("application/xml")
		public Response getTermFromURIXml(@QueryParam("uri") String uri) {
			Term term = null;
			try {term=services.getTerm(uri);}catch (Exception e){}
			if (term == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(term);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/xml/getTerm")
		public Response getTermFromURIXmlPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/getTerm")
		@Produces("application/json")
		public Response getTermFromURI(@QueryParam("uri") String uri) {
			Term term = null;
			try {term=services.getTerm(uri);}catch (Exception e){}
			if (term == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(term);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/getTerm")
		public Response getTermFromURIPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/getTermWithSubLevel")
		@Produces("application/json")
		public Response getTermAndLevelSubTerms(@QueryParam("uri") String termURI, @DefaultValue("1") @QueryParam("sublevel") int level) {
			
			Term term = null;
			try {term=services.getTermAndLevelSubTerms(termURI,level);}catch (Exception e){}
			if (term == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(term);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/getTermWithSubLevel")
		public Response getTermAndLevelSubTermsPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/xml/getTermWithSubLevel")
		@Produces("application/xml")
		public Response getTermAndLevelSubTermsXml(@QueryParam("uri") String termURI, @DefaultValue("1") @QueryParam("sublevel") int level) {

			Term term = null;
			try {term=services.getTermAndLevelSubTerms(termURI,level);}catch (Exception e){}
			if (term == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(term);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/xml/getTermWithSubLevel")
		public Response getTermAndLevelSubTermsXmlPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/searchSubTerms")
		@Produces("application/json")
		public Response searchSubTerms(@QueryParam("pattern") String key, @DefaultValue("1") @QueryParam("uri") String uri) {
			 
			 Set<SimpleTerm> res = null;
			try {res=services.searchSubTerms(key,uri);}catch (Exception e){}
			if (res == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(res);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/searchSubTerms")
		public Response searchSubTermsPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/xml/searchSubTerms")
		@Produces("application/xml")
		public Response searchSubTermsXml(@QueryParam("pattern") String key, @DefaultValue("1") @QueryParam("uri") String uri) {
			 
			 Set<SimpleTerm> res = null;
			try {res=services.searchSubTerms(key,uri);}catch (Exception e){res=null;}
			
			if (res == null)
				return RestTools.createNoResponse();
			else {
				 GenericEntity<Set<SimpleTerm>>  ge=new GenericEntity<Set<SimpleTerm>>(res){};
				 return RestTools.createResponse(ge);
			}
			
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/xml/searchSubTerms")
		public Response searchSubTermsXmlPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		
		
		@GET
		// @Produces("application/vnd.ms-excel")
		@Produces("application/*")
		@Path("/download")
		public Response getCodesForMapping(@DefaultValue("	") @QueryParam("separ") String separ,@DefaultValue("LEVELS") @QueryParam("type")  String type,@DefaultValue("0") @QueryParam("sublevels")  int sublevels) {
			char separc=separ.charAt(0);
			File rep = services.getCodes(separc,type,sublevels);
			if (rep == null)
				return RestTools.createNoResponse();
			return RestTools.createResponseForFile(rep);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/download")
		public Response getCodesForMappingPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		
		@GET
		// @Produces("application/vnd.ms-excel")
		@Produces("application/*")
		@Path("/exportRDF")
		public Response exportRDF( @QueryParam("uri")String uri) {
			
			File rep = services.getRDF(uri);
			if (rep == null)
				return RestTools.createNoResponse();
			return RestTools.createResponseForFile(rep);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/exportRDF")
		public Response exportRDFPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		
		
		@GET
		@Produces("application/json")
		@Path("/getErsatzConcept")
		public Response getErsatzConcept(
				@DefaultValue("deprecated") @QueryParam("key") String key,
				@DefaultValue("B04") @QueryParam("value") String value,
				@DefaultValue("urn:oid:2.16.840.1.113883.6.3") @QueryParam("codingSchemeURI") String codingSchemeURI
										) {
			ErsatzConcept c =null;
			try {
				c = services
					.getErsatzConcept(key, value, codingSchemeURI);}
			catch (Exception e){c=null;}
			
			if (c == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(c);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/getErsatzConcept")
		public Response getErsatzConceptPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("ehr4cr")
		@Produces("text/plain")
		public String getIt() {
			return "server is on. ";
		}
		
		
		@GET
		@Produces("application/json")
		@Path("ehr4cr/getErsatzConcept")
		public Response getErsatzConceptMapping(
				@DefaultValue("deprecated") @QueryParam("key") String key,
				@DefaultValue("B04") @QueryParam("value") String value,
				@DefaultValue("urn:oid:2.16.840.1.113883.6.3") @QueryParam("codingSchemeURI") String codingSchemeURI
										) {
				return this.getErsatzConcept(key, value, codingSchemeURI);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("ehr4cr/getErsatzConcept")
		public Response getErsatzConceptMappingPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@POST
		@Produces("application/json")
		@Consumes("application/json")
		@Path("/validate")
		public Response validate(
				HashSet<Term> set
										) {
		    
			if (set==null){
				System.out.println("validation issue ..the set is null");
			}
			else if (set.isEmpty()){
				System.out.println("validation issue ..the set is empty");
			}
			else {
				for (Term t:set)System.out.print(t.getSkosNotation()+" ");
				System.out.println("no reason to have issue");
			}
			Set<Term> ret=services.validate(set);
				if (ret == null)
					return RestTools.createNoResponse();
				return RestTools.createResponse(ret);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/validate")
		public Response validatePreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/xml/getDirectSupTerms")
		@Produces("application/xml")
		public Response getSupTermsXml(@QueryParam("uri") String uri) {
			Set<ErsatzTerm> terms = null;
			try {terms=services.getDirectSupTerms(uri);}catch (Exception e){}
			if (terms == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(terms);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/xml/getDirectSupTerms")
		public Response getDirectSupTermsXmlPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/getDirectSupTerms")
		@Produces("application/json")
		public Response getDirectSupTerms(@QueryParam("uri") String uri) {
			Set<ErsatzTerm> terms = null;
			try {terms=services.getDirectSupTerms(uri);}catch (Exception e){}
			if (terms == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(terms);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/getDirectSupTerms")
		public Response getDirectSupTermsPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/searchTerms")
		@Produces("application/json")
		public Response searchTerms(@QueryParam("pattern") String pattern, 
									@DefaultValue("code") @QueryParam("mode") String mode,
									@DefaultValue("none") @QueryParam("schemeUri") String schemeUri,
									@DefaultValue("none") @QueryParam("type") String type) {
			 
			 Set<ErsatzTerm> res = null;
			 boolean autocompletion=false;
			 if (schemeUri.equalsIgnoreCase("none"))schemeUri=null;
			 if (type.startsWith("auto"))autocompletion=true;
			try {res=services.searchTerms(pattern, mode, schemeUri,autocompletion);}catch (Exception e){e.printStackTrace();}
			if (res == null)
				return RestTools.createNoResponse();
			return RestTools.createResponse(res);
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/searchTerms")
		public Response searchTermsPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/xml/searchTerms")
		@Produces("application/xml")
		public Response searchTermsXml(@QueryParam("pattern") String pattern, 
									@DefaultValue("code") @QueryParam("mode") String mode,
									@DefaultValue("none") @QueryParam("schemeUri") String schemeUri,
									@DefaultValue("none") @QueryParam("type") String type) {
			 
			 return this.searchTerms(pattern, mode, schemeUri,type);
			
		}

		// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/xml/searchTerms")
		public Response searchTermsXmlPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
}
