package ehr4cr.rest.definitions;

import java.util.Set;

import ubiqore.graphs.semantic.SkosConcept;
import ubiqore.services.shared.ErsatzTerm;
import ubiqore.services.shared.Scheme;
import ubiqore.services.shared.Term;

import com.google.common.collect.Sets;

public class MappingTools {

	public static Scheme fillSchemeObject(ubiqore.graphs.semantic.Scheme o){
		if (o==null)return null;
		Scheme scheme=new Scheme();
		scheme.setLabel(o.getPreflabel().getLit().getLabel());
		scheme.setOid(o.getOid());
		scheme.setUri(o.getUri().stringValue());
		scheme.setUrn("urn:oid:"+o.getOid());
		scheme.setTopTerms(null);
		return scheme;
	}

	

	public static ErsatzTerm fillErsatzTerm(SkosConcept concept) {
		if (concept==null)return null;
		ErsatzTerm term=new ErsatzTerm();
		term.setSkosNotation(concept.getSkosNotation());
		term.setPreflabel(concept.getPreflabel().getLit().getLabel());
		term.setCodingSchemeOID(concept.getScheme().getOid());
		term.setEhr4crCodingSchemeURI(concept.getScheme().getUri().stringValue());
		term.setCodingSchemeName(concept.getScheme().getPreflabel().getLit().getLabel());
		term.setUri(concept.getUri().stringValue());
		if (concept.getBroaders()!=null){
			if (!concept.getBroaders().isEmpty()){
				Set<ErsatzTerm> broaders=Sets.newHashSet();
				for (SkosConcept s:concept.getBroaders()){
					broaders.add(fillErsatzTerm(s));
				}
				term.setTopBroaders(broaders);
			}
		}
		return term;
	}
	
	public static Term fillTerm(SkosConcept concept){
		if (concept==null)return null;
		Term term=new Term();
		Set<ErsatzTerm> eTerms=Sets.newHashSet();
		term.copy(MappingTools.fillErsatzTerm(concept));
		for (SkosConcept nar:concept.getNarrowers()){
			ErsatzTerm tmp=MappingTools.fillErsatzTerm(nar);
			eTerms.add(tmp);
		}
		term.setNarrowers(eTerms);
		return term;
	}
	
	
	public static SkosConcept fillSkos(Term term){
		if (term==null)return null;
		SkosConcept c=new SkosConcept();
		c.setSkosNotation(term.getSkosNotation());
		ubiqore.graphs.semantic.Scheme schem=new ubiqore.graphs.semantic.Scheme();
		schem.setOid(term.getCodingSchemeOID());
		c.setScheme(schem);
		return c;
	}
}
