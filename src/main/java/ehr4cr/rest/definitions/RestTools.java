package ehr4cr.rest.definitions;

import java.io.File;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

public class RestTools {

	 
	public static <T> Response createResponse (T o){
		ResponseBuilder re=Response.ok(o);
		
		re.header("Access-Control-Allow-Origin", "*");
		re.header("Access-Control-Allow-Methods", "POST, GET, UPDATE, OPTIONS");
		re.header("Access-Control-Allow-Headers", "x-http-method-override");
		return re.build();
	}
	
	public static <T> Response createResponseForHtml (T o){
		ResponseBuilder re=Response.ok(o,MediaType.APPLICATION_XHTML_XML);

		re.header("Access-Control-Allow-Origin", "*");
		re.header("Access-Control-Allow-Methods", "POST, GET, UPDATE, OPTIONS");
		re.header("Access-Control-Allow-Headers", "x-http-method-override");
		return re.build();
	}
	
	public static  Response createResponseForFile (File f){
		ResponseBuilder re=Response.ok(f,MediaType.APPLICATION_OCTET_STREAM);
		re.header("content-disposition", "attachment; filename =" + f.getName());
		re.header("Access-Control-Allow-Origin", "*");
		re.header("Access-Control-Allow-Methods", "POST, GET, UPDATE, OPTIONS");
		re.header("Access-Control-Allow-Headers", "x-http-method-override");
		return re.build();
	}
	
	
	public static <T> Response createNoResponse (){
		 ResponseBuilder re=Response.ok(new WrongMessage());
		re.header("Access-Control-Allow-Origin", "*");
		re.header("Access-Control-Allow-Methods", "POST, GET, UPDATE, OPTIONS");
		re.header("Access-Control-Allow-Headers", "x-http-method-override");
		return re.build();
	}
}