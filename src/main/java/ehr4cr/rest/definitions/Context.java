package ehr4cr.rest.definitions;

import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;

import com.google.common.base.Strings;

import ubiqore.services.shared.ErsatzTerm;

import ehr4cr.rest.api.Services;
import ehr4cr.rest.api.ServicesImpl;
import ehr4cr.rest.context.data.Ehr4crElement;




/* Definition of Context Services
 * Context Concepts are Data/Metadata/ValueSet ready to use concepts .
 * */
@Path("context")
public class Context {
	// Services are shared By the Parameters Class
	public static Services services=null;
	
	@javax.ws.rs.core.Context 
	private ServletContext sc;
	
	/*
	 * This service must be called for EHR4CR to be available
	 */
	@GET
	@Path("/create")
	@Produces("application/json")
	public Response create() {
		String contextPath=sc.getInitParameter("context");
		String resourcesPath=sc.getInitParameter("resources");
		Context.services=new ServicesImpl(contextPath,resourcesPath);
		Pivot.services=Context.services;
		
	     return RestTools.createResponse("ok");
	}
	
	// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/create")
		public Response createPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
	
	
	
	
	
	
	
	
	@GET
	@Produces("application/json")
	public Response getContext() {
		Ehr4crElement categoryElement = services.getRoot();
		if (categoryElement == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(categoryElement);
	}
	
	
	@GET
	@Path("/xml")
	@Produces("application/xml")
	public Response getContextHtml() {
		Ehr4crElement categoryElement = services.getRoot();
		if (categoryElement == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(categoryElement);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	public Response getContextPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Path("/element")
	@Produces("application/json")
	public Response getEhr4crElement(@QueryParam("uri") String uri) {
		Ehr4crElement element = null;
		try {element=services.getEhr4crElement(uri);}catch (Exception e){e.printStackTrace();}
		if (element == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(element);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/element")
	public Response getEhr4crElementPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Path("/xml/element")
	@Produces("application/xml")
	public Response getEhr4crElementXml(@QueryParam("uri") String uri) {
		return this.getEhr4crElement(uri);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("xml/element")
	public Response getEhr4crElementXmlPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Path("/search")
	@Produces("application/json")
	public Response searchElement(@QueryParam("pattern") String pattern) {
		List<Ehr4crElement> elements = null;
		try {elements=services.searchEhr4crElement(pattern);}catch (Exception e){e.printStackTrace();}
		if (elements == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(elements);
	}
	
	// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/search")
		public Response searchElementPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		
		@GET
		@Path("xml/search")
		@Produces("application/xml")
		public Response searchElementXml(@QueryParam("pattern") String pattern) {
			List<Ehr4crElement> elements = null;
			try {elements=services.searchEhr4crElement(pattern);}catch (Exception e){e.printStackTrace();}
			if (elements == null)
				return RestTools.createNoResponse();
			else {
				GenericEntity<List<Ehr4crElement>> ge=new 	GenericEntity<List<Ehr4crElement>>(elements) {};
				return RestTools.createResponse(ge);
			}
			
		}
		
		// The response for the preflight request made implicitly by the browser
			@OPTIONS
			@Path("xml/search")
			public Response searchElementXmlPreflight() {
				return Response
						.ok()
						.header("Access-Control-Allow-Origin", "*")
						.header("Access-Control-Allow-Methods",
								"POST, GET, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers",
								"x-http-method-override, X-Requested-With").build();

			}
			
			
			@GET
			@Path("/down")
			@Produces("application/json")
			public Response down(@QueryParam("p") String password) {
				String st="pass is wrong";
				if (password.equalsIgnoreCase("ab89")){
					try {
						services.disconnect();
						st="done";
					}catch (Exception e ){st="issue during disconnexion";}
					
				}
				
				return RestTools.createResponse(st);
			}
			
			// The response for the preflight request made implicitly by the browser
				@OPTIONS
				@Path("/down")
				public Response downPreflight() {
					return Response
							.ok()
							.header("Access-Control-Allow-Origin", "*")
							.header("Access-Control-Allow-Methods",
									"POST, GET, UPDATE, OPTIONS")
							.header("Access-Control-Allow-Headers",
									"x-http-method-override, X-Requested-With").build();

				}
				
				@GET
				@Path("xml/down")
				@Produces("application/xml")
				public Response downXml(@QueryParam("p") String password) {
					String st="pass is wrong";
					if (password.equalsIgnoreCase("ab89")){
						try {
							services.disconnect();
							st="done";
						}catch (Exception e ){st="issue during disconnexion";}
						
					}
					return RestTools.createResponse(st);
				}
				
				@GET
				@Path("/up")
				@Produces("application/json")
				public Response up(@QueryParam("p") String password) {
					String st="pass is wrong";
					if (password.equalsIgnoreCase("ab89")){
						try {
							services.reconnect();
							st="done";
						}catch (Exception e ){st="issue during re-connexion";}
						
					}
					
					return RestTools.createResponse(st);
				}
				
				// The response for the preflight request made implicitly by the browser
					@OPTIONS
					@Path("/up")
					public Response upPreflight() {
						return Response
								.ok()
								.header("Access-Control-Allow-Origin", "*")
								.header("Access-Control-Allow-Methods",
										"POST, GET, UPDATE, OPTIONS")
								.header("Access-Control-Allow-Headers",
										"x-http-method-override, X-Requested-With").build();

					}
					
					@GET
					@Path("xml/up")
					@Produces("application/xml")
					public Response upXml(@QueryParam("p") String password) {
						String st="pass is wrong";
						if (password.equalsIgnoreCase("ab89")){
							try {
								services.reconnect();
								st="done";
							}catch (Exception e ){st="issue during re-connexion";}
							
						}
						
						return RestTools.createResponse(st);
					}
					
					@GET
					@Path("/state")
					@Produces("application/json")
					public Response state(@QueryParam("p") String password) {
						String st="pass is wrong";
						if (password.equalsIgnoreCase("ab89")){
							try {
								
								st=services.state();
							}catch (Exception e ){st="issue during re-connexion";}
							
						}
						
						return RestTools.createResponse(st);
					}
					
					// The response for the preflight request made implicitly by the browser
						@OPTIONS
						@Path("/state")
						public Response statePreflight() {
							return Response
									.ok()
									.header("Access-Control-Allow-Origin", "*")
									.header("Access-Control-Allow-Methods",
											"POST, GET, UPDATE, OPTIONS")
									.header("Access-Control-Allow-Headers",
											"x-http-method-override, X-Requested-With").build();

						}
						
						@GET
						@Path("xml/state")
						@Produces("application/xml")
						public Response stateXml(@QueryParam("p") String password) {
							String st="pass is wrong";
							if (password.equalsIgnoreCase("ab89")){
								try {
									st=services.state();
								}catch (Exception e ){st="issue during re-connexion";}
								
							}
							
							return RestTools.createResponse(st);
						}
						
		@GET
		@Path("/validateSupTermsForMembers")
		@Produces("application/json")
		public Response validateSupTermsForMembers(@QueryParam("elementUri") String elementUri,@QueryParam("schemeOrTermUri") String schemeOrTermUri) {
			Set<ErsatzTerm> set=null;
			try {set=services.validateSupTermsForMembers(elementUri, schemeOrTermUri);}catch (Exception e){e.printStackTrace();}
			if (set == null || set.isEmpty())
				return RestTools.createNoResponse();
			return RestTools.createResponse(set);
		}
						
	// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("/validateSupTermsForMembers")
		public Response validateSupTermsForMembersPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		
		@GET
		@Path("xml/validateSupTermsForMembers")
		@Produces("application/xml")
		public Response validateSupTermsForMembersXml(@QueryParam("elementUri") String elementUri,
													  @QueryParam("schemeOrTermUri") String schemeOrTermUri) {
			return this.validateSupTermsForMembers(elementUri, schemeOrTermUri);
			
		}
							
	// The response for the preflight request made implicitly by the browser
		@OPTIONS
		@Path("xml/validateSupTermsForMembers")
		public Response validateSupTermsForMembersXmlXmlPreflight() {
			return Response
					.ok()
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods",
							"POST, GET, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers",
							"x-http-method-override, X-Requested-With").build();

		}
		
		@GET
		@Path("/query")
		@Produces("application/json")
		public Response query(	
				@QueryParam("pattern") String pattern,
				@DefaultValue("labelPattern") @QueryParam("type") String type,
				@DefaultValue("false") @QueryParam("exactMatch") String  exactMatch,
				@DefaultValue("false") @QueryParam("autocompletion") String  autocompletion,
				@DefaultValue("") @QueryParam("metadata") String  metadatas,
				@DefaultValue("") @QueryParam("memberOfUri") String  memberOfUris,
				@DefaultValue("") @QueryParam("topCategoryUri") String  topCategoryUris,
				@DefaultValue("") @QueryParam("schemeUri") String  schemeUris,
				@DefaultValue("10") @QueryParam("maxres") int  maxres,
				@DefaultValue("0") @QueryParam("offset") int  offset
				              ) {
			List<Ehr4crElement> elements = null;
			boolean exactMatchB=false;
			boolean autocompletionB=false;
			if (exactMatch.equalsIgnoreCase("ok")|| exactMatch.equalsIgnoreCase("true"))exactMatchB=true;
			if (autocompletion.equalsIgnoreCase("ok")|| autocompletion.equalsIgnoreCase("true"))autocompletionB=true;
			URI metadata=null;
			if (!Strings.isNullOrEmpty(metadatas))metadata=new URIImpl(metadatas);
			URI memberOfUri=null;
			if (!Strings.isNullOrEmpty(memberOfUris))memberOfUri=new URIImpl(memberOfUris);
			URI topCategoryUri=null;
			if (!Strings.isNullOrEmpty(topCategoryUris))topCategoryUri=new URIImpl(topCategoryUris);
			URI schemeUri=null;
			if (!Strings.isNullOrEmpty(schemeUris))schemeUri=new URIImpl(schemeUris);
			
			
			try {elements=services.searchEhr4crElementPro(pattern, 
											type, exactMatchB, autocompletionB, 
											metadata, memberOfUri, topCategoryUri, 
											schemeUri, maxres, offset);}catch (Exception e){e.printStackTrace();}
			if (elements == null || elements.isEmpty())
				return RestTools.createNoResponse();
			return RestTools.createResponse(elements);
		}
		
		// The response for the preflight request made implicitly by the browser
			@OPTIONS
			@Path("/query")
			public Response queryPreflight() {
				return Response
						.ok()
						.header("Access-Control-Allow-Origin", "*")
						.header("Access-Control-Allow-Methods",
								"POST, GET, UPDATE, OPTIONS")
						.header("Access-Control-Allow-Headers",
								"x-http-method-override, X-Requested-With").build();

			}
			
			
			@GET
			@Path("xml/query")
			@Produces("application/xml")
			public Response queryXml(
					@QueryParam("pattern") String pattern,
					@DefaultValue("labelPattern") @QueryParam("type") String type,
					@DefaultValue("false") @QueryParam("exactMatch") String  exactMatch,
					@DefaultValue("false") @QueryParam("autocompletion") String  autocompletion,
					@DefaultValue("") @QueryParam("metadata") String  metadatas,
					@DefaultValue("") @QueryParam("memberOfUri") String  memberOfUris,
					@DefaultValue("") @QueryParam("topCategoryUri") String  topCategoryUris,
					@DefaultValue("") @QueryParam("schemeUri") String  schemeUris,
					@DefaultValue("10") @QueryParam("maxres") int  maxres,
					@DefaultValue("0") @QueryParam("offset") int  offset
					) {
				System.out.println(pattern);
				
				List<Ehr4crElement> elements = null;
				boolean exactMatchB=false;
				boolean autocompletionB=false;
				if (exactMatch.equalsIgnoreCase("ok")|| exactMatch.equalsIgnoreCase("true"))exactMatchB=true;
				if (autocompletion.equalsIgnoreCase("ok")|| autocompletion.equalsIgnoreCase("true"))autocompletionB=true;
				URI metadata=null;
				if (!Strings.isNullOrEmpty(metadatas))metadata=new URIImpl(metadatas);
				URI memberOfUri=null;
				if (!Strings.isNullOrEmpty(memberOfUris))memberOfUri=new URIImpl(memberOfUris);
				URI topCategoryUri=null;
				if (!Strings.isNullOrEmpty(topCategoryUris))topCategoryUri=new URIImpl(topCategoryUris);
				URI schemeUri=null;
				if (!Strings.isNullOrEmpty(schemeUris))schemeUri=new URIImpl(schemeUris);
				
				
				try {elements=services.searchEhr4crElementPro(pattern, 
												type, exactMatchB, autocompletionB, 
												metadata, memberOfUri, topCategoryUri, 
												schemeUri, maxres, offset);}catch (Exception e){e.printStackTrace();}
				
				
				if (elements == null || elements.isEmpty())
					return RestTools.createNoResponse();
				else {
					GenericEntity<List<Ehr4crElement>> ge=new 	GenericEntity<List<Ehr4crElement>>(elements) {};
					return RestTools.createResponse(ge);
				}
				
			}
			
			// The response for the preflight request made implicitly by the browser
				@OPTIONS
				@Path("xml/query")
				public Response queryXmlPreflight() {
					return Response
							.ok()
							.header("Access-Control-Allow-Origin", "*")
							.header("Access-Control-Allow-Methods",
									"POST, GET, UPDATE, OPTIONS")
							.header("Access-Control-Allow-Headers",
									"x-http-method-override, X-Requested-With").build();

				}
}
