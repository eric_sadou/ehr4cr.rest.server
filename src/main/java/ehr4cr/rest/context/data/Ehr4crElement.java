package ehr4cr.rest.context.data;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import ubiqore.services.shared.ErsatzTerm;
import ubiqore.services.shared.Term;

@XmlRootElement
public class Ehr4crElement {

	@Override
	public String toString() {
		return "Ehr4crElement [metadata=" + metadata + ", topCategoryUri="
				+ topCategoryUri + ", uri=" + uri + ", memberOfUri="
				+ memberOfUri + ", members=" + members + ", relatedTerm="
				+ relatedTerm + ", label=" + label + ", value=" + value
				+ ", unit=" + unit + "]";
	}

	String metadata;
	String topCategoryUri;
	String uri;
	String memberOfUri;
	ArrayList<Ehr4crElement> members;
	ErsatzTerm relatedTerm;
	String label; 
	Float value;
	Term unit;  

	public void addMember(Ehr4crElement uri) {
		if (members == null)
			members = new ArrayList<Ehr4crElement>();
		members.add(uri);
	}

	public String getLabel() {
		return label;
	}

	public String getMemberOfUri() {
		return memberOfUri;
	}

	public ArrayList<Ehr4crElement> getMembers() {
		return members;
	}

	public String getMetadata() {
		return metadata;
	}

	public ErsatzTerm getRelatedTerm() {
		return relatedTerm;
	}
	public String getTopCategoryUri() {
		return topCategoryUri;
	}

	public Term getUnit() {
		return unit;
	}
	public String getUri() {
		return uri;
	}

	public Float getValue() {
		return value;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setMemberOfUri(String memberOfUri) {
		this.memberOfUri = memberOfUri;
	}

	public void setMembers(ArrayList<Ehr4crElement> membersUri) {
		this.members = membersUri;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public void setRelatedTerm(ErsatzTerm relatedTerm) {
		this.relatedTerm = relatedTerm;
	}

	public void setTopCategoryUri(String topCategoryUri) {
		this.topCategoryUri = topCategoryUri;
	}

	public void setUnit(Term unit) {
		this.unit = unit;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public void setValue(Float value) {
		this.value = value;
	}

}
