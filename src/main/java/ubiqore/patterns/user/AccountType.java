package ubiqore.patterns.user;

public class AccountType {
String label;
public static final String ADMIN="admin";
public static final String MANAGER="manager";
public static final String USER="user";
public static final String VIEWER="viewer";
public String getLabel() {
	return label;
}
public void setLabel(String label) {
	this.label = label;
}

}
