package ubiqore.patterns.concept;

public class Property {
	private String type;
	private String name;
    private String value;
    
    
	public Property(){}
	public Property(String name,String value){
    	this.name=name;
    	this.value=value;
    }
	
	public Property(String name,String value,String type){
    	this.name=name;
    	this.value=value;
    	this.type=type;
    }
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
