package ubiqore.patterns.concept;

import java.util.ArrayList;

public class AbstractConcept {

	protected String label = "";
	protected String urn = "";
	protected String id = "";
	protected String codingSchemeURI ="";
	protected ArrayList<Property> properties = new ArrayList<Property>();
	public AbstractConcept() {

	}
	
	public boolean addProperty(Property e) {
		return properties.add(e);
	}

	public void copy(AbstractConcept ac) {
		this.id = ac.getId();
		this.label = ac.getLabel();
		this.urn = ac.getUrn();
		this.codingSchemeURI=ac.getCodingSchemeURI();
		this.properties = ac.getProperties();
	}

	public String getCodingSchemeURI() {
		return codingSchemeURI;
	}

	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public ArrayList<Property> getProperties() {
		return properties;
	}

	public String getUrn() {
		return urn;
	}

	//

	public void setCodingSchemeURI(String codingSchemeURI) {
		this.codingSchemeURI = codingSchemeURI;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setProperties(ArrayList<Property> properties) {
		this.properties = properties;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public String toString() {
		String st = this.getId() + ";";
		st+=this.getLabel()+";";
		st+=this.getCodingSchemeURI()+";";
		for (Property p : this.getProperties()) {
			if (p.getType() == null)
				st += p.getName() + ":" + p.getValue() + ";";
			else
				st += p.getName() + " [" + p.getType() + "]" + ":"
						+ p.getValue() + ";";
		}
		return st;
	}
}
