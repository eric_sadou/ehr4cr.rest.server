package ubiqore.patterns.concept;

import java.util.ArrayList;
import java.util.Set;

import com.google.common.collect.Sets;

public class Concept extends AbstractConcept{
    
	public Concept(){
		
	}
	public void copy(ErsatzConcept e){
		AbstractConcept a=(AbstractConcept)e;
		AbstractConcept o=this;
		o=a;
	}
	private Set<ErsatzConcept> parents=Sets.newHashSet();
	
	public boolean addParent(ErsatzConcept e) {
		return parents.add(e);
	}
	public Set<ErsatzConcept> getParents() {
		return parents;
	}
	public void setParents(Set<ErsatzConcept> fathers) {
		this.parents = fathers;
	}
	public Set<ErsatzConcept> getChildren() {
		return children;
	}
	public void setChildren(Set<ErsatzConcept> sons) {
		this.children = sons;
	}
	private Set<ErsatzConcept> children=Sets.newHashSet();
	public boolean addChild(ErsatzConcept e) {
		return children.add(e);
	}
	
	public String toString(){
		String st=this.getId()+";";
		for (Property p:this.getProperties()){
			if (p.getType()==null)st+=p.getName()+":"+p.getValue()+";";
			else st+=p.getName()+" ["+p.getType()+"]"+":"+p.getValue()+";"+"\n";
		}
		for (ErsatzConcept son:this.getChildren()){
			st+="MyChild:"+son.toString()+"\n";
		}
		for (ErsatzConcept father:this.getParents()){
			st+="MyParent:"+father.toString()+"\n";
		}
		for (Relation  rel:this.getRelationsTo()){
			st+="sourceOf:"+rel.getName()+"-->target:"+rel.getTarget().toString()+"\n";
		}
		for (Relation  rel:this.getRelationsFrom()){
			st+="targetOf:"+rel.getName()+"<--source:"+rel.getSource().toString()+"\n";
		}
		
		return st;
	}
	
	private Set<Relation> relationsTo=Sets.newHashSet(); // the concept is the source of these relations.
	public Set<Relation> getRelationsTo() {
		return relationsTo;
	}
	public void setRelationsTo(Set<Relation> relationsTo) {
		this.relationsTo = relationsTo;
	}
	public Set<Relation> getRelationsFrom() {
		return relationsFrom;
	}
	public void setRelationsFrom(Set<Relation> relationsFrom) {
		this.relationsFrom = relationsFrom;
	}
	private Set<Relation> relationsFrom=Sets.newHashSet(); // the concept is the target of these relations.

	public boolean addRelationsFrom(Relation arg0) {
		return relationsFrom.add(arg0);
	}
	public boolean addRelationsTo(Relation arg0) {
		return relationsTo.add(arg0);
	}
}
