package ubiqore.patterns.concept;

import java.util.ArrayList;

public class Relation {
	private String urn;
	private String name="";
	
	private  ArrayList<String> otherName=new  ArrayList<String> ();
	private AbstractConcept target=null;
	private AbstractConcept source=null;
	public Relation(){}
	public boolean addOtherName(String e) {
		return otherName.add(e);
	}
	
	public String getName() {
		return name;
	}
	public ArrayList<String> getOtherName() {
		return otherName;
	}
	public AbstractConcept getSource() {
		return source;
	}
	public AbstractConcept getTarget() {
		return target;
	}
	public String getUrn() {
		return urn;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setOtherName(ArrayList<String> otherName) {
		this.otherName = otherName;
	}
	public void setSource(AbstractConcept source) {
		this.source = source;
	}
	public void setTarget(AbstractConcept target) {
		this.target = target;
	}
	public void setUrn(String urn) {
		this.urn = urn;
	}
}
