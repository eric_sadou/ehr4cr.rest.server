package ubiqore.patterns.concept;

public class SimpleConcept {

	String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCodingSchemeURI() {
		return codingSchemeURI;
	}
	public void setCodingSchemeURI(String codingSchemeURI) {
		this.codingSchemeURI = codingSchemeURI;
	}
	String label;
	String codingSchemeURI;
	
	@Override
	public boolean equals(Object obj) { 
		if (this == obj)return true;
		if (!(obj instanceof SimpleConcept))return false;
		SimpleConcept sc=(SimpleConcept)obj;
		
		if (this.getId().equalsIgnoreCase(sc.getId())){
			if (this.getCodingSchemeURI().equalsIgnoreCase(sc.getCodingSchemeURI())){
				return true;
			}
		}
	    return false; 
	  }
}
