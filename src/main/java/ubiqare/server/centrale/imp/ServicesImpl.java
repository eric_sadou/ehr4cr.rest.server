package ubiqare.server.centrale.imp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.PreDestroy;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.sail.SailException;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.Resources;
import com.tinkerpop.blueprints.Vertex;

import ehr4cr.rest.api.Services;
import ehr4cr.rest.context.data.Ehr4crElement;

/*
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.elasticsearch.common.collect.Lists;

import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import ubiqare.server.QuerySearchAnswer;
import ubiqare.server.centrale.api.Services;
import ubiqore.patterns.concept.Concept;
import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.patterns.concept.AbstractConcept;
import ubiqore.patterns.concept.Property;
import ubiqore.patterns.concept.SimpleConcept;
import ubiqore.term.graphs.sem.SemanticTerminology;
import ubiqore.term.graphs.sem.UbiqGraph;
import ubiqore.term.graphs.sem.ValueSetElement;
import ubiqore.terminology.graphs.core.UbiKey;
import ubiqore.terminology.graphs.imp.fullTerminology.ComposedTerminologiesGraph;
import ubiqore.terminology.graphs.imp.fullTerminology.TermArtefact;
import ubiqore.terminology.graphs.imp.fullTerminology.UnitRelationship;
import ubiqore.terminology.graphs.imp.fullTerminology.ValueSetRelationship;
import ubiqore.terminology.indexing.tools.IndexManaging;
import ubiqore.terminology.indexing.tools.IndexQueryAnswser;
import ubiqore.tinkerpop.availables.impls.Implementation;*/


import ubiqore.graphs.semantic.ContextVocabulary;
import ubiqore.graphs.semantic.DataElement;
import ubiqore.graphs.semantic.ContextElementsGraph;
import ubiqore.graphs.semantic.MultiTerminologyGraph;
import ubiqore.graphs.semantic.SkosConcept;
import ubiqore.graphs.semantic.ValueSetElement;
import ubiqore.graphs.semantic.ValueSetGraph;
import ubiqore.patterns.concept.Concept;
import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.patterns.concept.Property;

@Deprecated
public class ServicesImpl  {
	
	public File exportXMLRDF(){
		init();
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss") ;
		String rdfFileName=dateFormat.format(date)+".rdf";
		String rdfPath="/Reborn/tmp/"+rdfFileName;
		try {
			this.ressources.exportRDF(rdfPath);
		} catch (SailException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String zipName=dateFormat.format(date)+".zip";
		try {
			this.zipFile(rdfPath,"export.rdf","/Reborn/tmp/"+zipName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new File( "/Reborn/tmp/"+zipName);
	}
	public File getCodesForMapping(char separ,String type,int sublevels){
		init();
		Set<String> set=Sets.newHashSet();
		List<String> list=Lists.newArrayList();
		String dataTypeName="data";
		List<SkosConcept> l=Lists.newArrayList();
		try {
			if (type.equalsIgnoreCase(ContextVocabulary.EXPANDED)){
				dataTypeName=ContextVocabulary.EXPANDED;
				Set<URI> uris=ressources.getConceptsURI();
				for (URI uri:uris)l.add(ressources.getConcept(uri));
			}
			else if (type.equalsIgnoreCase(ContextVocabulary.FULL ) ||
				(type.equalsIgnoreCase(ContextVocabulary.LEVELS)&& sublevels>0)
			   ){
				if (type.equalsIgnoreCase(ContextVocabulary.FULL )){
					sublevels=0; // 0 means ALL the sublevels for this service.
					dataTypeName=ContextVocabulary.FULL;
				}else {
					dataTypeName=ContextVocabulary.LEVELS;
				}
				
				Set<String> s=ressources.getNarrowersBest(sublevels);
				for (String st:s)l.add(ressources.getConcept(new URIImpl(st)));
			}else { // LEVELS + 0 means only single list.
				l=ressources.getOriginalList();
				dataTypeName="original";
			}
			
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (SkosConcept s:l){
			this.addToSet(separ,s,set);
		}
		
		System.out.println("nbLines to write="+set.size());
		String fileName="/Reborn/tmp/tmpfile";
		
		File zip=new File(this.show(fileName,dataTypeName,set));
		return zip;
	}
	
	private void zipFile(String filePath,String fileName,String zipPath) throws IOException{
		
		FileInputStream in = new FileInputStream(filePath);
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipPath));
        // name the file inside the zip  file 

        try {
			out.putNextEntry(new ZipEntry(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

           byte[] b = new byte[1024];

       int count;

       while ((count = in.read(b)) > 0) {
           System.out.println();

        out.write(b, 0, count);
       }
       out.close();
       in.close();
       }
	 
	@Deprecated
	private void addToSet(char separ,SkosConcept s, Set<String>  set){
		set.add("urn:oid:"+s.getScheme().getOid()+separ+s.getScheme().getDef()+separ+s.getSkosNotation()+separ+s.getPreflabel().getLit().getLabel());
		
	}
	@Deprecated
	public  String show(String filePath,String dataTypeName,Set<String> set){
		File file=new File(filePath);
		
		PrintStream console = System.out;

		
		FileOutputStream fos=null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PrintStream ps = new PrintStream(fos);
		System.setOut(ps);
		//System.out.println("This goes to out.txt");

		
		Iterator<String> ite=set.iterator();
		List<String> list=Lists.newArrayList();
		
		while (ite.hasNext()){
			list.add(ite.next());
			// System.out.println(ite.next());
		}
		Collections.sort(list,String.CASE_INSENSITIVE_ORDER);
		for (String st:list){
			System.out.println(st);
		}
		
		System.setOut(console);
		
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss") ;
		String zipName=dateFormat.format(date)+".zip";
		try {
			this.zipFile(filePath,dataTypeName+".tsv","/Reborn/tmp/"+zipName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "/Reborn/tmp/"+zipName;
	}
	
	
	
	
	@Deprecated
	public static void main (String ... args){
		ServicesImpl imp=null;
		try {
		imp=new ServicesImpl();
		imp.init();
		Set<String> subs= imp.ressources.getTransitiveSubClasses("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/71388002");
		//173382008
		for (String sub:subs){
			System.out.println(imp.ressources.getConcept(new URIImpl(sub)).getPreflabel().getLit().getLabel());
		}
		imp.getCodesForMapping('	',"EXPANDED",0);
		/*Ehr4crElement  e=imp.getRoot();
		System.out.println(e.getLabel());*/
		//Map<String, ArrayList<Ehr4crElement>>  test =imp.searchValueSetElement("proced");
		//Iterator<Entry<String, ArrayList<Ehr4crElement>>> ite=test.entrySet().iterator();
		
		
		}catch (Exception e ){
			e.printStackTrace();
		}
		finally {
			imp.disconnect("neo4j");
		}
		/*Set<ErsatzConcept> br=imp.getBranches(ALL);
		String formatbranches="";
		for (ErsatzConcept ec:br){
			formatbranches+="\n Branch for ALL:"+ec.getLabel()+ " - "+ec.toString();
		}
		System.out.println("branches for ALL "+  formatbranches);
		
		Set<ErsatzConcept> br2=imp.getBranches("atc.2012");
		String formatbranches2="";
		for (ErsatzConcept ec:br2){
			formatbranches2+="\n Branch for atc.2012:"+ec.getLabel();
		}
		System.out.println("branches for ATC"+  formatbranches2);*/
		
		//System.out.println(imp.getConcept("URN", "urn:uuid:53637510-4905-4919-862f-b58012c398f2","ALL").toString());
	/*	System.out.println(imp.getErsatzConcept("URN", "urn:uuid:80391197-1e89-434a-8fb2-1ffeda851d1f","ALL").toString());
		
		Set<SimpleConcept> set=imp.getSubConceptsList("EXTERNAL_ID", "138875005","ALL",3);
		for (SimpleConcept sc:set){
			System.out.println("kid -->"+sc.getLabel()+"  "+sc.getId());*/
		
		
		//System.out.println(imp.getConcept("DESCRIPTION", "oxyquinoline","atc.2012"));
		//System.out.println(imp.getConcept("EXTERNAL_ID", "D08AH03","atc.2012"));
		
		
	}
	/*private static IndexManaging index = new IndexManaging(null,
			"server.ubiqore.com", 9300, "pivot", "vertex", null);
	
	public static ComposedTerminologiesGraph terminology = new ComposedTerminologiesGraph(
			Implementation.REXSTER,
			"http://server.ubiqore.com:8182/graphs/ehr4cr.pivot", true, null);*/

	
    public static ContextElementsGraph ehr4cr=null;
    
    public static MultiTerminologyGraph ressources=null;

    /* should be called all the time */
    @Deprecated
    public void init(){
    	if (ehr4cr==null){
    		try {
				ehr4cr=new ContextElementsGraph(false,"/Reborn/data/ehr4cr/graphs/11022013de");
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	if (ressources==null){
    		try {
				ressources= new MultiTerminologyGraph(false,"/Reborn/data/ehr4cr/graphs/11022013");
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    
    
    @Deprecated
    @PreDestroy
    protected void BeforeKillConnection(){
    	System.out.println("Destroy the Services !!!!!!!!!!!!!!!");
    	this.disconnect("neo4j");
    }
 
	
	/*public Ehr4crElement getRoot() {
		init();
		// TODO Auto-generated method stub
		try {
			ehr4cr.statements();
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//DataElement vse= ehr4cr.getRoot();
		return this.fill(vse, null);
		
	}*/
	
	public  Term getTermExpansion(String uri,int limit){
		init();
		try {
			SkosConcept concept =ressources.getConcept(new URIImpl(uri));
			concept=ressources.getAllNarrowers(concept, limit, true);
			Term term=this.fill(concept);
			return term;
		} catch (SailException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (MalformedQueryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (QueryEvaluationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		return null;
	}
	
	
	public Ehr4crElement getDataElement(String uri) {
		init();
		// TODO Auto-generated method stub
		try {
			DataElement vse= ehr4cr.getDataElementByURI(new URIImpl(uri),true);
			SkosConcept concept =null;
			if (vse.getRelatedCD()!=null){
				concept =ressources.getConcept(new URIImpl(vse.getRelatedCD()));
			}
			return this.fill(vse, concept);
			
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String disconnect(String pass) {
		if (!pass.equalsIgnoreCase("neo4j"))return "wrong password";
		init();
		ehr4cr.disconnect();
		ressources.disconnect();
	//	ressources.disconnect();
		return "disconnect";
	}
	
	
	public String connect(String pass) {
		if (!pass.equalsIgnoreCase("neo4j"))return "wrong password";
		init();
		ehr4cr.reconnect();
		ressources.reconnect();
		return "connect";
	}
	
	public ErsatzConcept getErsatzConcept(String key, String value,
			String codingSchemeURI) {
		init();
		ErsatzConcept ec=new ErsatzConcept();
		SkosConcept concept=ressources.getErsatzConcept(value,codingSchemeURI);
		
		ec.setCodingSchemeURI(codingSchemeURI); // on renvoie le meme codeSchemeURI transmis
		ArrayList<Property> a=Lists.newArrayList();
		Property p=new Property();
		p.setName("codingSchemeName");
		p.setValue(concept.getScheme().getPreflabel().getLit().getLabel());
		
		
		a.add(p);
		ec.setProperties(a);
		// evolution possible : tester codingSchemeURI et toujours renvoyer sous la form urn:oid ...
		ec.setId(concept.getSkosNotation());
		ec.setLabel(concept.getPreflabel().getLit().getLabel());
		ec.setUrn(concept.getUri().stringValue());
		return ec;
	}

	private Ehr4crElement fill(DataElement vse,SkosConcept concept){
		Ehr4crElement el=new Ehr4crElement();
		el.setLabel(vse.getLabel());
		el.setMemberOfUri(vse.getMemberOfUri());
		el.setTopCategoryUri(vse.getTopCategoryURI());
		if (vse.getMembers()!=null){
			
			for (DataElement member: vse.getMembers()){
				try {
					el.addMember(this.fill(member, ressources.getConcept(new URIImpl(member.getRelatedCD()))));
				} catch (SailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MalformedQueryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (QueryEvaluationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		el.setMetadata(vse.getMetadata());
		el.setValue(vse.getCoValue());
		Unit unit=new Unit();
		//unit.setUcumValue(vse.getPqUnit());
		//el.setUnit(unit);
		//el.setUri(vse.getUri());
		//if (concept!=null)el.setRelatedTerm(this.fill(concept));
		return el;
	}
	
	private Term fill(SkosConcept concept){
		Term term=new Term();
		term.setCodingSchemeOID(concept.getScheme().getOid());
		//term.setPathLexChapter(concept.getPathLexChapter());
		term.setPreflabel(concept.getPreflabel().getLit().getLabel());
		term.setInternalcodingSchemeURI(concept.getScheme().getUri().toString());
		term.setSkosNotation(concept.getSkosNotation());
		term.setUri(concept.getUri().stringValue());
		//term.setVersion(concept.getVersion());
		if (concept.getNarrowers()!=null){
			for (SkosConcept fils:concept.getNarrowers()){
				term.addNarrower(this.fill(fils));
			}
		}
		return term;
	}

	
	public List<Ehr4crElement> searchDataElement(String str) {
		init();
		List<Ehr4crElement> l=Lists.newArrayList();
		try {
			Map<URI, String> uris=ehr4cr.searchStringREGEX(str);
			if (uris==null)return l;
			if (uris.isEmpty())return l;
			Iterator<Entry<URI, String>> ite=uris.entrySet().iterator();
			while (ite.hasNext()){
				Entry<URI, String> ent=ite.next();
				l.add(this.getDataElement(ent.getKey().stringValue()));
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}


	
	public List<ErsatzConcept> searchSubTerms(String key, String domainURI) {
		// TODO Auto-generated method stub
		return null;
	}


	
	public List<ErsatzConcept> getSubTerms(String domainURI) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Ehr4crElement getDataElement(EntityNameOrUri uri) {
		System.out.println(uri.toString());
		if (uri.getValue() instanceof String){
			System.out.println("URI !!");
			
		}else if (uri.getValue()instanceof EntityName) System.out.println("ID !!!");
		else {
			System.out.println();
		}
		return null;
	}
	
}
	/*
	/**
	 * Check ElasticSearch index and return from Graph Rexster access, based on
	 * URN.
	 *//*
	public ErsatzConcept getErsatzConcept(String key, String value,String codingSchemeURI) {
		
		ErsatzConcept ec=this.getErsatzConceptFromGraph(key, value, codingSchemeURI);
		
		if (ec!=null)return ec;
		
		
		value="\""+value+"\"";
		
		// TODO Auto-generated method stub
		List<String> l = index.get(key, value, null); // Strings are URN
														// (UbiKey.URN)
		if (l == null || l.isEmpty())
			return null;

		ErsatzConcept ret = new ErsatzConcept();
		
		if (!codingSchemeURI.equalsIgnoreCase(ALL)){
			for (String id:l){
				Vertex v = terminology.getEntity(UbiKey.URN.toString(), id);
				if (v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
					terminology.fillConcept(v, ret);
					return ret;
				}
			}
			return null;
		}else {
			System.out.println(l.get(0));
			Vertex v = terminology.getEntity(UbiKey.URN.toString(), l.get(0));
			terminology.fillConcept(v, ret);
		}
		return ret;
	}
	
	
	public Set<SimpleConcept> getSubConceptsList(String key, String value,String codingSchemeURI,int level){
		AbstractConcept e = this.getErsatzConcept(key, value,codingSchemeURI);
		
		if (e==null)return null;
		
		Vertex v = terminology.getEntity(UbiKey.URN.toString(), e.getUrn());
		
		HashSet<SimpleConcept> mySet=Sets.newHashSet();
		if (v==null)return null;
		else {
			this.addRecursiveSub(v, mySet,level);
		}
		return mySet;
	}

	public Concept getConcept(String key, String value,String codingSchemeURI) {
		Concept c = new Concept();

		AbstractConcept e = this.getErsatzConcept(key, value,codingSchemeURI);
		if (e==null)return null;
		
		c.copy(e);
		
		//c.copy(e); // recupere le tronc commun. (les def.)
		System.out.println("copy ok ? "+c.getUrn());
		Vertex v = terminology.getEntity(UbiKey.URN.toString(), c.getUrn());
		this.addSupConcepts(v, c);
		this.addSubConcepts(v, c);

		return c;
	}

	public Set<ErsatzConcept> getBranches(String codingSchemeURI) {
		try {
			Set<ErsatzConcept> set = Sets.newHashSet();

			Iterator<Vertex> ite = terminology.getEntities(
					UbiKey.ARTEFACT.toString(),
					TermArtefact.CONCEPT_BRANCH.toString());
			
			while (ite.hasNext()) {
				Vertex v = ite.next();
				ErsatzConcept e = new ErsatzConcept();
				terminology.fillConcept(v, e);
				if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
				set.add(e);
				}

			}
			return set;
		} catch (Exception e) {
			return null;
		}

	}

	public Concept luckySearch(String search,String codingSchemeURI) {
		List<String> urns=index.get(null, search, null);
		if (urns.isEmpty())return null;
		
		if (codingSchemeURI.equalsIgnoreCase(ALL))
			return this.getConcept(UbiKey.URN.toString(), urns.get(0),ALL);
		else {
			for (String urn:urns){
				Vertex v = terminology.getEntity(UbiKey.URN.toString(), urn);
				if (v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
					return this.getConcept(UbiKey.URN.toString(), urn,codingSchemeURI);
				}
			}
		}
		return null;
	}
	
	
	public List<ErsatzConcept> searchConceptByKey(String key, String value,
			String codingSchemeURI) {
		if (key.equalsIgnoreCase(UbiKey.HASVALUESET.toString())){
			int i=0;
			List<ErsatzConcept> l=new ArrayList<ErsatzConcept>();
			Iterator<Vertex> ite=terminology.getEntities(UbiKey.HASVALUESET.toString(), value);
			if (ite!=null){
					while (ite.hasNext()){
						Vertex v=ite.next();
						ErsatzConcept c=new ErsatzConcept();
						terminology.fillConcept(v, c);
						if (v.getProperty("CodingSchemeURI")!=null){
							if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
								l.add(c);
							}
						}else {System.out.println("some strange concept exists on server : vertexId="+v.getId());}
					}
				return l;
			}
			return null;
			
		}
		else {
			List<ErsatzConcept> l=Lists.newArrayList();
			List<String> urns=index.get(key, value, null);
			if (urns.isEmpty())return null;
			else {
				for (String urn:urns){
					Vertex v = terminology.getEntity(UbiKey.URN.toString(), urn);
					ErsatzConcept c=new ErsatzConcept();
					terminology.fillConcept(v, c);
					if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
						l.add(c);
					}
				}
			}
			return l;
		}
		
		
	}
	
	public List<ErsatzConcept> searchConcept(String search,String codingSchemeURI) {
		List<ErsatzConcept> l=Lists.newArrayList();
		List<String> urns=index.get(null, search, null);
		if (urns.isEmpty())return l;
		else {
			for (String urn:urns){
				Vertex v = terminology.getEntity(UbiKey.URN.toString(), urn);
				ErsatzConcept c=new ErsatzConcept();
				terminology.fillConcept(v, c);
				if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
					l.add(c);
				}
			}
		}
		return l;
	}

	private void addSupConcepts(Vertex v, Concept c) {
		try {
			Iterator<Edge> it = v
					.getEdges(Direction.OUT, UbiKey.ISA.toString()).iterator();
			while (it.hasNext()) {
				Vertex vIn = it.next().getVertex(Direction.IN);
				String artefact = vIn.getProperty(UbiKey.ARTEFACT.toString())
						.toString();
				if (artefact.equalsIgnoreCase(TermArtefact.CONCEPT.toString())
						|| artefact
								.equalsIgnoreCase(TermArtefact.CONCEPT_BRANCH
										.toString())) {
					ErsatzConcept supper = new ErsatzConcept();
					terminology.fillConcept(vIn, supper);
					c.addParent(supper);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void fillSimpleConcept(Vertex v,SimpleConcept sc){
		sc.setId(v.getProperty(UbiKey.EXTERNAL_ID.toString()).toString());
		sc.setLabel(v.getProperty(UbiKey.DESCRIPTION.toString()).toString());
		sc.setCodingSchemeURI(v.getProperty(UbiKey.CodingSchemeURI.toString()).toString());
	}
	
	// ajout des fils dans le set a plat...
	private void addRecursiveSub(Vertex v,HashSet<SimpleConcept> mySet,int level){
		if (level==1)return; // 
		if (level<0)level=0;
		
		if (mySet==null)return;
		
		try {
			Iterator<Edge> it = v.getEdges(Direction.IN, UbiKey.ISA.toString())
					.iterator();
			while (it.hasNext()) {
				Vertex vOut = it.next().getVertex(Direction.OUT);
				String artefact = vOut.getProperty(UbiKey.ARTEFACT.toString())
						.toString();
				if (artefact.equalsIgnoreCase(TermArtefact.CONCEPT.toString()))
						 {
					SimpleConcept sc=new SimpleConcept();
					this.fillSimpleConcept(vOut, sc);
					//System.out.println("level "+ level+ " " +sc.getLabel());
					mySet.add(sc);
					this.addRecursiveSub(vOut, mySet,level-1);
				}
			}
		} catch (Exception e) {
			
		}
	}
	
	private void addSubConcepts(Vertex v, Concept c) {
		try {
			Iterator<Edge> it = v.getEdges(Direction.IN, UbiKey.ISA.toString())
					.iterator();
			while (it.hasNext()) {
				Vertex vOut = it.next().getVertex(Direction.OUT);
				String artefact = vOut.getProperty(UbiKey.ARTEFACT.toString())
						.toString();
				if (artefact.equalsIgnoreCase(TermArtefact.CONCEPT.toString())
						|| artefact
								.equalsIgnoreCase(TermArtefact.CONCEPT_BRANCH
										.toString())) {
					ErsatzConcept sub = new ErsatzConcept();
					terminology.fillConcept(vOut, sub);
					c.addChild(sub);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public boolean linkConceptToValueSet(String conceptUrn, String valueSetUrn,boolean replace) {
		ValueSetRelationship vsr=new ValueSetRelationship();
		vsr.setConceptURN(conceptUrn);
		vsr.setValueSetURN(valueSetUrn);
		vsr.setReplace(replace);
		if (terminology.addEntity(vsr,false).getCode()==100)return true;
		else return false;
	}
	
	public boolean linkConceptToUnit(String conceptUrn, String unitUrn,
			boolean replace) {
		UnitRelationship vsr=new UnitRelationship();
		vsr.setConceptURN(conceptUrn);
		vsr.setUnitURN(unitUrn);
		vsr.setReplace(replace);
		if (terminology.addEntity(vsr,false).getCode()==100)return true;
		else return false;
	}
	
	public QuerySearchAnswer searchConceptWithLimit(String search,
			String codingSchemeURI, int limit,int from) {
		List<ErsatzConcept> l=Lists.newArrayList();
		IndexQueryAnswser iqa=index.getPagination(null, search, null,limit,from); // 0 --> default (v 3.3.6 of ubiqore.terminology) 
		if (iqa.getUrns().isEmpty())return new QuerySearchAnswer();
		else {
			for (String urn:iqa.getUrns()){
				Vertex v = terminology.getEntity(UbiKey.URN.toString(), urn);
				ErsatzConcept c=new ErsatzConcept();
				terminology.fillConcept(v, c);
				if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
					l.add(c);
				}
			}
		}
		QuerySearchAnswer qsa=new QuerySearchAnswer();
		qsa.setErsatzConcepts(l);
		qsa.setTotalHit(iqa.getTotalHits());
		qsa.setMessage(qsa.getTotalHit()+ " -> total answer. but "+qsa.getErsatzConcepts().size()+" are sent.");
		return qsa;
	}

	

}
*/