package ubiqare.server.centrale.imp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.annotation.PreDestroy;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.sail.SailException;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tinkerpop.blueprints.Vertex;

import ehr4cr.rest.api.Services;
import ehr4cr.rest.context.data.Ehr4crElement;

/*
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.elasticsearch.common.collect.Lists;

import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

import ubiqare.server.QuerySearchAnswer;
import ubiqare.server.centrale.api.Services;
import ubiqore.patterns.concept.Concept;
import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.patterns.concept.AbstractConcept;
import ubiqore.patterns.concept.Property;
import ubiqore.patterns.concept.SimpleConcept;
import ubiqore.term.graphs.sem.SemanticTerminology;
import ubiqore.term.graphs.sem.UbiqGraph;
import ubiqore.term.graphs.sem.ValueSetElement;
import ubiqore.terminology.graphs.core.UbiKey;
import ubiqore.terminology.graphs.imp.fullTerminology.ComposedTerminologiesGraph;
import ubiqore.terminology.graphs.imp.fullTerminology.TermArtefact;
import ubiqore.terminology.graphs.imp.fullTerminology.UnitRelationship;
import ubiqore.terminology.graphs.imp.fullTerminology.ValueSetRelationship;
import ubiqore.terminology.indexing.tools.IndexManaging;
import ubiqore.terminology.indexing.tools.IndexQueryAnswser;
import ubiqore.tinkerpop.availables.impls.Implementation;*/


import ubiqore.graphs.semantic.MultiTerminologyGraph;
import ubiqore.graphs.semantic.SkosConcept;
import ubiqore.graphs.semantic.ValueSetElement;
import ubiqore.graphs.semantic.ValueSetGraph;
import ubiqore.patterns.concept.Concept;
import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.patterns.concept.Property;


@Deprecated
public class ServicesImplWithVSG  {
	
	public static void main (String ... args){
		ServicesImplWithVSG imp=null;
		try {
		imp=new ServicesImplWithVSG();
		/*Ehr4crElement  e=imp.getRoot();
		System.out.println(e.getLabel());*/
		//Map<String, ArrayList<Ehr4crElement>>  test =imp.searchValueSetElement("proced");
		//Iterator<Entry<String, ArrayList<Ehr4crElement>>> ite=test.entrySet().iterator();
		
		
		}catch (Exception e ){
			e.printStackTrace();
		}
		finally {
			imp.disconnect();
		}
		/*Set<ErsatzConcept> br=imp.getBranches(ALL);
		String formatbranches="";
		for (ErsatzConcept ec:br){
			formatbranches+="\n Branch for ALL:"+ec.getLabel()+ " - "+ec.toString();
		}
		System.out.println("branches for ALL "+  formatbranches);
		
		Set<ErsatzConcept> br2=imp.getBranches("atc.2012");
		String formatbranches2="";
		for (ErsatzConcept ec:br2){
			formatbranches2+="\n Branch for atc.2012:"+ec.getLabel();
		}
		System.out.println("branches for ATC"+  formatbranches2);*/
		
		//System.out.println(imp.getConcept("URN", "urn:uuid:53637510-4905-4919-862f-b58012c398f2","ALL").toString());
	/*	System.out.println(imp.getErsatzConcept("URN", "urn:uuid:80391197-1e89-434a-8fb2-1ffeda851d1f","ALL").toString());
		
		Set<SimpleConcept> set=imp.getSubConceptsList("EXTERNAL_ID", "138875005","ALL",3);
		for (SimpleConcept sc:set){
			System.out.println("kid -->"+sc.getLabel()+"  "+sc.getId());*/
		
		
		//System.out.println(imp.getConcept("DESCRIPTION", "oxyquinoline","atc.2012"));
		//System.out.println(imp.getConcept("EXTERNAL_ID", "D08AH03","atc.2012"));
		
		
	}
	/*private static IndexManaging index = new IndexManaging(null,
			"server.ubiqore.com", 9300, "pivot", "vertex", null);
	
	public static ComposedTerminologiesGraph terminology = new ComposedTerminologiesGraph(
			Implementation.REXSTER,
			"http://server.ubiqore.com:8182/graphs/ehr4cr.pivot", true, null);*/

	
    public static ValueSetGraph ehr4cr=null;
    
    public static MultiTerminologyGraph ressources=null;

    /* should be called all the time */
    public void init(){
    	if (ehr4cr==null){
    		try {
				ehr4cr=new ValueSetGraph(false,"/Reborn/data/ehr4cr/graphsTest/vseBis");
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	if (ressources==null){
    		try {
				ressources= new MultiTerminologyGraph(false,"/Reborn/data/ehr4cr/graphsTest/multiBis");
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    
    @Deprecated
    private ArrayList<Ehr4crElement> getFromConceptURI(String uri){
    	ArrayList<Ehr4crElement> answer=Lists.newArrayList();
    	try {
			List<ValueSetElement> l=ehr4cr.getValueSetFromConceptUri(uri);
			for (ValueSetElement vse:l){
				Ehr4crElement e=this.fill(vse,ressources.getConcept(new URIImpl(uri)));
				answer.add(e);
			}
			if (answer.isEmpty()){
				Set<String> b=ressources.getBroaders(uri);
				for (String st:b){
					answer=this.getFromConceptURI(st);
				}
			}
			if (answer.isEmpty()){
				Set<String> n=ressources.getNarrowers(uri);
				for (String st:n){
					answer=this.getFromConceptURI(st);
				}
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return answer;
    }
    
    @PreDestroy
    protected void BeforeKillConnection(){
    	System.out.println("Destroy the Services !!!!!!!!!!!!!!!");
    	this.disconnect();
    }
 
	
	public Ehr4crElement getRoot() {
		init();
		// TODO Auto-generated method stub
		try {
			ehr4cr.statements();
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ValueSetElement vse= ehr4cr.getRoot();
		return this.fill(vse, null);
		
	}
	
	public  Term getTermExpansion(String uri,int limit){
		init();
		try {
			SkosConcept concept =ressources.getConcept(new URIImpl(uri));
			concept=ressources.getAllNarrowers(concept, limit, true);
			Term term=this.fill(concept);
			return term;
		} catch (SailException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (MalformedQueryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (QueryEvaluationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		return null;
	}
	
	public Ehr4crElement getValueSetElement(String uri) {
		init();
		// TODO Auto-generated method stub
		try {
			ValueSetElement vse= ehr4cr.getValueSetByURI(new URIImpl(uri),true);
			SkosConcept concept =ressources.getConcept(new URIImpl(vse.getRelatedToUri()));
			return this.fill(vse, concept);
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String disconnect() {
		init();
		ehr4cr.disconnect();
		ressources.disconnect();
	//	ressources.disconnect();
		return "disconnect";
	}
	
	public String connect() {
		init();
		ehr4cr.reconnect();
		ressources.reconnect();
		return "connect";
	}
	
	public ErsatzConcept getErsatzConcept(String key, String value,
			String codingSchemeURI) {
		init();
		ErsatzConcept ec=new ErsatzConcept();
		SkosConcept concept=ressources.getErsatzConcept(value,codingSchemeURI);
		
		ec.setCodingSchemeURI(codingSchemeURI); // on renvoie le meme codeSchemeURI transmis
		ArrayList<Property> a=Lists.newArrayList();
		Property p=new Property();
		p.setName("codingSchemeName");
		p.setValue(concept.getScheme().getPreflabel().getLit().getLabel());
		
		
		a.add(p);
		ec.setProperties(a);
		// evolution possible : tester codingSchemeURI et toujours renvoyer sous la form urn:oid ...
		ec.setId(concept.getSkosNotation());
		ec.setLabel(concept.getPreflabel().getLit().getLabel());
		ec.setUrn(concept.getUri().stringValue());
		return ec;
	}

	private Ehr4crElement fill(ValueSetElement vse,SkosConcept concept){
		Ehr4crElement el=new Ehr4crElement();
		el.setLabel(vse.getLabel());
		el.setMemberOfUri(vse.getMemberOfUri());
		el.setTopCategoryUri(vse.getTopCategoryURI());
		if (vse.getMembersUri()!=null){
			
			for (ValueSetElement member: vse.getMembersUri()){
				try {
					el.addMember(this.fill(member, ressources.getConcept(new URIImpl(member.getRelatedToUri()))));
				} catch (SailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MalformedQueryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (QueryEvaluationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		el.setMetadata(vse.getMetadata());
		//el.setRankURI(vse.getRankURI());
		//el.setUnitURI(vse.getUnitURI());
		el.setUri(vse.getUri());
		//if (concept!=null)el.setRelatedTerm(this.fill(concept));
		return el;
	}
	
	private Term fill(SkosConcept concept){
		Term term=new Term();
		/*term.setCodingSchemeURI(concept.getScheme().getOid());
		term.setPathLexChapter(concept.getPathLexChapter());
		term.setPreflabel(concept.getPreflabel().getLit().getLabel());
		term.setScheme(concept.getScheme().getUri().toString());
		term.setSkosNotation(concept.getSkosNotation());
		term.setUri(concept.getUri().stringValue());
		term.setVersion(concept.getVersion());
		if (concept.getNarrowers()!=null){
			for (SkosConcept fils:concept.getNarrowers()){
				term.addNarrower(this.fill(fils));
			}
		}*/
		return term;
	}

	
	public List<Ehr4crElement> searchValueSetElement(String str) {
		init();
		List<Ehr4crElement> l=Lists.newArrayList();
		try {
			Map<URI, String> uris=ehr4cr.searchStringREGEX(str);
			if (uris==null)return l;
			if (uris.isEmpty())return l;
			Iterator<Entry<URI, String>> ite=uris.entrySet().iterator();
			while (ite.hasNext()){
				Entry<URI, String> ent=ite.next();
				l.add(this.getValueSetElement(ent.getKey().stringValue()));
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}
	
}
	/*
	/**
	 * Check ElasticSearch index and return from Graph Rexster access, based on
	 * URN.
	 *//*
	public ErsatzConcept getErsatzConcept(String key, String value,String codingSchemeURI) {
		
		ErsatzConcept ec=this.getErsatzConceptFromGraph(key, value, codingSchemeURI);
		
		if (ec!=null)return ec;
		
		
		value="\""+value+"\"";
		
		// TODO Auto-generated method stub
		List<String> l = index.get(key, value, null); // Strings are URN
														// (UbiKey.URN)
		if (l == null || l.isEmpty())
			return null;

		ErsatzConcept ret = new ErsatzConcept();
		
		if (!codingSchemeURI.equalsIgnoreCase(ALL)){
			for (String id:l){
				Vertex v = terminology.getEntity(UbiKey.URN.toString(), id);
				if (v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
					terminology.fillConcept(v, ret);
					return ret;
				}
			}
			return null;
		}else {
			System.out.println(l.get(0));
			Vertex v = terminology.getEntity(UbiKey.URN.toString(), l.get(0));
			terminology.fillConcept(v, ret);
		}
		return ret;
	}
	
	
	public Set<SimpleConcept> getSubConceptsList(String key, String value,String codingSchemeURI,int level){
		AbstractConcept e = this.getErsatzConcept(key, value,codingSchemeURI);
		
		if (e==null)return null;
		
		Vertex v = terminology.getEntity(UbiKey.URN.toString(), e.getUrn());
		
		HashSet<SimpleConcept> mySet=Sets.newHashSet();
		if (v==null)return null;
		else {
			this.addRecursiveSub(v, mySet,level);
		}
		return mySet;
	}

	public Concept getConcept(String key, String value,String codingSchemeURI) {
		Concept c = new Concept();

		AbstractConcept e = this.getErsatzConcept(key, value,codingSchemeURI);
		if (e==null)return null;
		
		c.copy(e);
		
		//c.copy(e); // recupere le tronc commun. (les def.)
		System.out.println("copy ok ? "+c.getUrn());
		Vertex v = terminology.getEntity(UbiKey.URN.toString(), c.getUrn());
		this.addSupConcepts(v, c);
		this.addSubConcepts(v, c);

		return c;
	}

	public Set<ErsatzConcept> getBranches(String codingSchemeURI) {
		try {
			Set<ErsatzConcept> set = Sets.newHashSet();

			Iterator<Vertex> ite = terminology.getEntities(
					UbiKey.ARTEFACT.toString(),
					TermArtefact.CONCEPT_BRANCH.toString());
			
			while (ite.hasNext()) {
				Vertex v = ite.next();
				ErsatzConcept e = new ErsatzConcept();
				terminology.fillConcept(v, e);
				if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
				set.add(e);
				}

			}
			return set;
		} catch (Exception e) {
			return null;
		}

	}

	public Concept luckySearch(String search,String codingSchemeURI) {
		List<String> urns=index.get(null, search, null);
		if (urns.isEmpty())return null;
		
		if (codingSchemeURI.equalsIgnoreCase(ALL))
			return this.getConcept(UbiKey.URN.toString(), urns.get(0),ALL);
		else {
			for (String urn:urns){
				Vertex v = terminology.getEntity(UbiKey.URN.toString(), urn);
				if (v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
					return this.getConcept(UbiKey.URN.toString(), urn,codingSchemeURI);
				}
			}
		}
		return null;
	}
	
	@Override
	public List<ErsatzConcept> searchConceptByKey(String key, String value,
			String codingSchemeURI) {
		if (key.equalsIgnoreCase(UbiKey.HASVALUESET.toString())){
			int i=0;
			List<ErsatzConcept> l=new ArrayList<ErsatzConcept>();
			Iterator<Vertex> ite=terminology.getEntities(UbiKey.HASVALUESET.toString(), value);
			if (ite!=null){
					while (ite.hasNext()){
						Vertex v=ite.next();
						ErsatzConcept c=new ErsatzConcept();
						terminology.fillConcept(v, c);
						if (v.getProperty("CodingSchemeURI")!=null){
							if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
								l.add(c);
							}
						}else {System.out.println("some strange concept exists on server : vertexId="+v.getId());}
					}
				return l;
			}
			return null;
			
		}
		else {
			List<ErsatzConcept> l=Lists.newArrayList();
			List<String> urns=index.get(key, value, null);
			if (urns.isEmpty())return null;
			else {
				for (String urn:urns){
					Vertex v = terminology.getEntity(UbiKey.URN.toString(), urn);
					ErsatzConcept c=new ErsatzConcept();
					terminology.fillConcept(v, c);
					if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
						l.add(c);
					}
				}
			}
			return l;
		}
		
		
	}
	
	public List<ErsatzConcept> searchConcept(String search,String codingSchemeURI) {
		List<ErsatzConcept> l=Lists.newArrayList();
		List<String> urns=index.get(null, search, null);
		if (urns.isEmpty())return l;
		else {
			for (String urn:urns){
				Vertex v = terminology.getEntity(UbiKey.URN.toString(), urn);
				ErsatzConcept c=new ErsatzConcept();
				terminology.fillConcept(v, c);
				if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
					l.add(c);
				}
			}
		}
		return l;
	}

	private void addSupConcepts(Vertex v, Concept c) {
		try {
			Iterator<Edge> it = v
					.getEdges(Direction.OUT, UbiKey.ISA.toString()).iterator();
			while (it.hasNext()) {
				Vertex vIn = it.next().getVertex(Direction.IN);
				String artefact = vIn.getProperty(UbiKey.ARTEFACT.toString())
						.toString();
				if (artefact.equalsIgnoreCase(TermArtefact.CONCEPT.toString())
						|| artefact
								.equalsIgnoreCase(TermArtefact.CONCEPT_BRANCH
										.toString())) {
					ErsatzConcept supper = new ErsatzConcept();
					terminology.fillConcept(vIn, supper);
					c.addParent(supper);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void fillSimpleConcept(Vertex v,SimpleConcept sc){
		sc.setId(v.getProperty(UbiKey.EXTERNAL_ID.toString()).toString());
		sc.setLabel(v.getProperty(UbiKey.DESCRIPTION.toString()).toString());
		sc.setCodingSchemeURI(v.getProperty(UbiKey.CodingSchemeURI.toString()).toString());
	}
	
	// ajout des fils dans le set a plat...
	private void addRecursiveSub(Vertex v,HashSet<SimpleConcept> mySet,int level){
		if (level==1)return; // 
		if (level<0)level=0;
		
		if (mySet==null)return;
		
		try {
			Iterator<Edge> it = v.getEdges(Direction.IN, UbiKey.ISA.toString())
					.iterator();
			while (it.hasNext()) {
				Vertex vOut = it.next().getVertex(Direction.OUT);
				String artefact = vOut.getProperty(UbiKey.ARTEFACT.toString())
						.toString();
				if (artefact.equalsIgnoreCase(TermArtefact.CONCEPT.toString()))
						 {
					SimpleConcept sc=new SimpleConcept();
					this.fillSimpleConcept(vOut, sc);
					//System.out.println("level "+ level+ " " +sc.getLabel());
					mySet.add(sc);
					this.addRecursiveSub(vOut, mySet,level-1);
				}
			}
		} catch (Exception e) {
			
		}
	}
	
	private void addSubConcepts(Vertex v, Concept c) {
		try {
			Iterator<Edge> it = v.getEdges(Direction.IN, UbiKey.ISA.toString())
					.iterator();
			while (it.hasNext()) {
				Vertex vOut = it.next().getVertex(Direction.OUT);
				String artefact = vOut.getProperty(UbiKey.ARTEFACT.toString())
						.toString();
				if (artefact.equalsIgnoreCase(TermArtefact.CONCEPT.toString())
						|| artefact
								.equalsIgnoreCase(TermArtefact.CONCEPT_BRANCH
										.toString())) {
					ErsatzConcept sub = new ErsatzConcept();
					terminology.fillConcept(vOut, sub);
					c.addChild(sub);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean linkConceptToValueSet(String conceptUrn, String valueSetUrn,boolean replace) {
		ValueSetRelationship vsr=new ValueSetRelationship();
		vsr.setConceptURN(conceptUrn);
		vsr.setValueSetURN(valueSetUrn);
		vsr.setReplace(replace);
		if (terminology.addEntity(vsr,false).getCode()==100)return true;
		else return false;
	}
	@Override
	public boolean linkConceptToUnit(String conceptUrn, String unitUrn,
			boolean replace) {
		UnitRelationship vsr=new UnitRelationship();
		vsr.setConceptURN(conceptUrn);
		vsr.setUnitURN(unitUrn);
		vsr.setReplace(replace);
		if (terminology.addEntity(vsr,false).getCode()==100)return true;
		else return false;
	}
	@Override
	public QuerySearchAnswer searchConceptWithLimit(String search,
			String codingSchemeURI, int limit,int from) {
		List<ErsatzConcept> l=Lists.newArrayList();
		IndexQueryAnswser iqa=index.getPagination(null, search, null,limit,from); // 0 --> default (v 3.3.6 of ubiqore.terminology) 
		if (iqa.getUrns().isEmpty())return new QuerySearchAnswer();
		else {
			for (String urn:iqa.getUrns()){
				Vertex v = terminology.getEntity(UbiKey.URN.toString(), urn);
				ErsatzConcept c=new ErsatzConcept();
				terminology.fillConcept(v, c);
				if (codingSchemeURI.equalsIgnoreCase(ALL)||v.getProperty("CodingSchemeURI").toString().equalsIgnoreCase(codingSchemeURI)){
					l.add(c);
				}
			}
		}
		QuerySearchAnswer qsa=new QuerySearchAnswer();
		qsa.setErsatzConcepts(l);
		qsa.setTotalHit(iqa.getTotalHits());
		qsa.setMessage(qsa.getTotalHit()+ " -> total answer. but "+qsa.getErsatzConcepts().size()+" are sent.");
		return qsa;
	}

	

}
*/