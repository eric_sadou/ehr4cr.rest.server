package ubiqare.server.centrale.imp;

import java.util.Set;

import com.google.common.collect.Sets;

/*
 * Term is used to access Term
 */
public class Term {

	String preflabel;

	String uri;

	String codingSchemeOID;

	String skosNotation; // unique internal code.

	String internalcodingSchemeURI;

	Set<Term> narrowers = Sets.newHashSet();

	public void addNarrower(Term term) {
		this.narrowers.add(term);
	}

	public String getCodingSchemeOID() {
		return codingSchemeOID;
	}

	public String getInternalcodingSchemeURI() {
		return internalcodingSchemeURI;
	}

	public Set<Term> getNarrowers() {
		return narrowers;
	}

	public String getPreflabel() {
		return preflabel;
	}
	public String getSkosNotation() {
		return skosNotation;
	}

	public String getUri() {
		return uri;
	}

	public void setCodingSchemeOID(String CodingSchemeOID) {
		this.codingSchemeOID = CodingSchemeOID;
	}

	public void setInternalcodingSchemeURI(String scheme) {
		this.internalcodingSchemeURI = scheme;
	}

	/*
	 * String pathLexChapter; public String getPathLexChapter() { return
	 * pathLexChapter; } public void setPathLexChapter(String pathLexChapter) {
	 * this.pathLexChapter = pathLexChapter; }
	 */

	public void setNarrowers(Set<Term> narrowers) {
		this.narrowers = narrowers;
	}

	public void setPreflabel(String preflabel) {
		this.preflabel = preflabel;
	}

	public void setSkosNotation(String skosNotation) {
		this.skosNotation = skosNotation;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

}
