package ubiqare.server.centrale.imp;

import java.io.Serializable;


public class EntityNameOrUri implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public EntityNameOrUri(String uri){
		this.uri=uri;
	}
	String uri;
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public EntityName getEntityName() {
		return entityName;
	}
	public void setEntityName(EntityName entityName) {
		this.entityName = entityName;
	}
	EntityName entityName;
	public Object getValue(){
		if (this.uri!=null)return this.uri;
		else if (this.entityName!=null)return this.entityName;
		else return (String)null;
	}
	@Override
	public String toString(){
		String st=uri;
		if (st==null)st=this.entityName.getNamespace()+"-"+this.getEntityName().getName();
		return st;
		
	}
}
