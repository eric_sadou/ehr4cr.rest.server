package ubiqare.server.centrale.imp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;

import ubiqare.server.MyResource;

import ubiqore.patterns.concept.AbstractConcept;
import ubiqore.patterns.concept.Concept;
import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.patterns.concept.Property;


public class ValueSetServicesImpl {
// public class ValueSetServicesImpl implements ValueSetServices {
	
	/*public static void main(String ...args){
		ValueSetServicesImpl imp=new ValueSetServicesImpl();
		ValueSet vs=imp.getValueSet("EXTERNAL_ID", "SNOMEDCT_263495000", 5);
		System.out.println(vs.toString());
		System.out.println(imp.getLevelValue("EXTERNAL_ID", "248152002", 0).toString());
		System.out.println(imp.getLevelValue("EXTERNAL_ID", "248152002", 1).toString());
		System.out.println(imp.getCategories().toString());
		System.out.println(imp.getSystemRoot().toString());
		System.out.println(imp.isSystemReadyToUse());
		
	}
	private static ValueSetGraph valuesetSystem = new ValueSetGraph(
			Implementation.REXSTER,
			"http://server.ubiqore.com:8182/graphs/valueset", false, null);
	
	private static IndexManaging index = new IndexManaging(null,
			"server.ubiqore.com", 9300, "valueset09", "vertex", null);
	
	
	 *  return names of categories inside the valuesetSystem...
	 *  (version 2.0 TODO) provide all the categories ... not important
	 * @see ubiqare.valueSet.services.ValueSetServices#getCategories()
	 
	@Override
	public HashSet<CategoryValueSet> getCategories() {
		try {
			HashSet<CategoryValueSet> set=Sets.newHashSet();
			Iterator<Vertex> categories=valuesetSystem.getEntitiesBasedOnArtefact(ValueSetArtefact.VALUESET_CATEGORY);
			while (categories.hasNext()){
				Vertex v=categories.next();
				CategoryValueSet cvs=this.getCategoryValueSet(UbiKey.EXTERNAL_ID.toString(), v.getProperty(UbiKey.EXTERNAL_ID.toString()).toString());
				set.add(cvs);
			}
			return set;
		}catch (Exception e){}
		
		return null;
	}
	
	 * return category information and Branch Inside informations (ValueSet which can be ValueSet or CategoryValueSET ...)
	 * (non-Javadoc)
	 * @see ubiqare.valueSet.services.ValueSetServices#getCategoryValueSet(java.lang.String, java.lang.String)
	 
	@Override
	public CategoryValueSet getCategoryValueSet(String key , String value) {
		// key could be "label" / "externalId" / "urn" 
		
		try {
			String  urn=null;
			if (!key.equalsIgnoreCase(UbiKey.URN.toString())){
				Iterator<String> urns=null;
				urns=index.get(key, value, null).iterator(); // retourn les urn.
				urn=urns.next();
			}
			else {
				urn=value;
			}
			
			Vertex v=valuesetSystem.getEntity(UbiKey.URN.toString(),urn);
			if (v.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.VALUESET_CATEGORY.toString())){
				CategoryValueSet csv=new CategoryValueSet();
				csv.setExternalId(v.getProperty(UbiKey.EXTERNAL_ID.toString()).toString());
				csv.setLabel(v.getProperty(UbiKey.DESCRIPTION.toString()).toString());
				csv.setUrn(v.getProperty(UbiKey.URN.toString()).toString());
				Iterator<Edge> ite=v.getEdges(Direction.IN, UbiKey.ISA.toString()).iterator();
				ArrayList<ValueSet> vsl=Lists.newArrayList();
				while (ite.hasNext()){
					Edge link=ite.next();
					Vertex vOUT=link.getVertex(Direction.OUT);
					ValueSet vsF=new ValueSet();
					if (vOUT.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.VALUESET_CATEGORY.toString())){
						// l'objet cré est bien un categoryValueSet....
						vsF=new CategoryValueSet();
						
					}
					if (vOUT.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.UNIT.toString())){
						// l'objet cré est bien un categoryValueSet....
						vsF=new Unit();
						((Unit)vsF).setUnit(vOUT.getProperty(UbiKey.UNIT.toString()).toString());
					}
					vsF.setExternalId(vOUT.getProperty(UbiKey.EXTERNAL_ID.toString()).toString());
					vsF.setLabel(vOUT.getProperty(UbiKey.DESCRIPTION.toString()).toString());
					vsF.setUrn(vOUT.getProperty(UbiKey.URN.toString()).toString());
					vsl.add(vsF); 
				}
				csv.setBranch(vsl); 
				return csv;
			}
		}catch (Exception e ){
			return null;
		}
		
		
		return null;
	}

	@Override
	public ValueSet getFullValueSet(String urn) {
		// TODO Auto-generated method stub
		return this.getValueSet(UbiKey.URN.toString(), urn,0);
	}

	@Override
	
	 * return of Value with or without children depending arg level
	 * @see ubiqare.valueSet.services.ValueSetServices#getLevelValue(java.lang.String, java.lang.String, int)
	 
	public LevelValue getLevelValue(String key, String value, int level) {
		LevelValue lv=null;
		if (level<0)return null;
		
		try {
			String urn=null;
			if (!key.equalsIgnoreCase(UbiKey.URN.toString())){
			Iterator<String> vertices=index.get(key, value, null).iterator(); // retourn les urn.
			urn=vertices.next();
			}
			else urn=value;
			Vertex v=valuesetSystem.getEntity(UbiKey.URN.toString(),urn);
			if (v.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.VALUE.toString())){
				lv=new LevelValue(level);
				lv.setExternalId(v.getProperty(UbiKey.EXTERNAL_ID.toString()).toString());
				lv.setLabel(v.getProperty(UbiKey.DESCRIPTION.toString()).toString());
				lv.setUrn(v.getProperty(UbiKey.URN.toString()).toString());
				lv.setTerminologyUrn(v.getProperty("CodingSchemeURI").toString());
			
			if (level==1){
				return lv;
			}
			Concept c=MyResource.services.getConcept(UbiKey.EXTERNAL_ID.toString(),lv.getExternalId(),lv.getTerminologyUrn());
			for (ErsatzConcept ec:c.getChildren()){
				int newLevel=0;
		 		if (level>0)newLevel=level-1;
				lv.getChildren().add(this.getLevelValue(ec,newLevel));
			}
			
		}
		}catch (Exception e){e.printStackTrace();return null;}
		return lv;
	}
	
	private LevelValue getLevelValue(ErsatzConcept ec,int level){
		if (level<0 )return null;
		LevelValue lv=new LevelValue(level);
		Concept c=MyResource.services.getConcept(UbiKey.URN.toString(),ec.getUrn(),ubiqare.server.centrale.api.Services.ALL);
		lv.setExternalId(c.getId());
		lv.setLabel(c.getLabel());
		lv.setUrn(c.getUrn());
		lv.setTerminologyUrn(c.getCodingSchemeURI());
		if (level==1)return lv;
		else {
			for (ErsatzConcept fils:c.getChildren()){
				lv.getChildren().add(this.getLevelValue(fils, level-1));
			}
			return lv;
		}
		
	}

	
	
	public CategoryValueSet getSystemRootLight() {
		CategoryValueSet root=null;
		try {
		Vertex v=valuesetSystem.getEntitiesBasedOnArtefact(ValueSetArtefact.VALUESET_ROOT).next();
		root=new CategoryValueSet();
		root.setExternalId("root");
		root.setLabel(v.getProperty(UbiKey.DESCRIPTION.toString()).toString());
		root.setVersion(v.getProperty(UbiKey.VERSION.toString()).toString());
		root.setUrn(v.getProperty(UbiKey.URN.toString()).toString());
		Iterator<Edge> ite=v.getEdges(Direction.IN,UbiKey.IS_BRANCH_OF.toString()).iterator();
		while (ite.hasNext()){
			Vertex vB=ite.next().getVertex(Direction.OUT);
			if (vB.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.VALUESET_CATEGORY.toString())){
			root.getBranch().add(this.getCategoryValueSet(UbiKey.URN.toString(), vB.getProperty(UbiKey.URN.toString()).toString()));
			}
			else if (vB.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.VALUESET.toString())){
				root.getBranch().add(this.getValueSet(UbiKey.URN.toString(), vB.getProperty(UbiKey.URN.toString()).toString(),1));
			}
		}
		}
		catch (Exception e ){e.printStackTrace();root=null;}
		
		return root;
	}
	
	
	
	
	@Override
	public String getVSSVersion(){
		
		try {
		Vertex v=valuesetSystem.getEntitiesBasedOnArtefact(ValueSetArtefact.VALUESET_ROOT).next();
		return v.getProperty(UbiKey.VERSION.toString()).toString();
		}catch (Exception e ){
			return "null";
		}
		
	}
	@Override
	public CategoryValueSet getSystemRoot() {
		CategoryValueSet root=null;
		try {
		Vertex v=valuesetSystem.getEntitiesBasedOnArtefact(ValueSetArtefact.VALUESET_ROOT).next();
		root=new CategoryValueSet();
		root.setExternalId("root");
		root.setLabel(v.getProperty(UbiKey.DESCRIPTION.toString()).toString());
		root.setUrn(v.getProperty(UbiKey.URN.toString()).toString());
		root.setVersion(v.getProperty(UbiKey.VERSION.toString()).toString());
		Iterator<Edge> ite=v.getEdges(Direction.IN,UbiKey.IS_BRANCH_OF.toString()).iterator();
		while (ite.hasNext()){
			Vertex vB=ite.next().getVertex(Direction.OUT);
			if (vB.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.VALUESET_CATEGORY.toString())){
			root.getBranch().add(this.getCategoryValueSet(UbiKey.URN.toString(), vB.getProperty(UbiKey.URN.toString()).toString()));
			}
			else if (vB.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.VALUESET.toString())){
				root.getBranch().add(this.getValueSet(UbiKey.URN.toString(), vB.getProperty(UbiKey.URN.toString()).toString(),1));
			}
		}
		}
		catch (Exception e ){e.printStackTrace();root=null;}
		return root;
	}

	@Override
	public ValueSet getValueSet(String key, String value, int level) {
		ValueSet vs=new ValueSet();
		if (level<0)return null;
		
		else {
			try{Vertex v=null;
				if (key.equalsIgnoreCase(UbiKey.URN.toString())){
					 v=this.valuesetSystem.getEntity(UbiKey.URN.toString(),value);
				}else {
				Iterator<String> vertices=index.get(key, value, null).iterator(); // retourn les urn.
				String  urn=vertices.next();
					v=this.valuesetSystem.getEntity(UbiKey.URN.toString(),urn);
				}
				if (v.getProperty(UbiKey.ARTEFACT.toString()).toString().equalsIgnoreCase(ValueSetArtefact.VALUESET.toString())){
					vs.setExternalId(v.getProperty(UbiKey.EXTERNAL_ID.toString()).toString());
					vs.setLabel(v.getProperty(UbiKey.DESCRIPTION.toString()).toString());
					vs.setUrn(v.getProperty(UbiKey.URN.toString()).toString());
					Iterator<Edge> links=v.getEdges(Direction.IN, UbiKey.ISA.toString()).iterator();
					HashSet<LevelValue> sets=Sets.newHashSet();
					while (links.hasNext()){
						Edge link=links.next();
						Vertex va=link.getVertex(Direction.OUT);
						LevelValue vl=new LevelValue(level);
						String test=va.getProperty(UbiKey.EXTERNAL_ID.toString()).toString();
						//test=test.replaceAll("SNOMEDCT_","");
						vl.setExternalId(test);
						vl.setLabel(va.getProperty(UbiKey.DESCRIPTION.toString()).toString());
						vl.setUrn(va.getProperty(UbiKey.URN.toString()).toString());
						vl.setVersion(va.getProperty(UbiKey.VERSION.toString()).toString());
						vl.setTerminologyUrn(va.getProperty("CodingSchemeURI").toString());
						this.addChildren(vl);
						sets.add(vl);
					}
					vs.setBag(sets);
				}
				else if (v.getProperty(UbiKey.ARTEFACT.toString()).toString().endsWith(ValueSetArtefact.UNIT.toString())){
					vs=new Unit();
					vs.setExternalId(v.getProperty(UbiKey.EXTERNAL_ID.toString()).toString());
					vs.setLabel(v.getProperty(UbiKey.DESCRIPTION.toString()).toString());
					vs.setUrn(v.getProperty(UbiKey.URN.toString()).toString());
					((Unit)vs).setUnit(v.getProperty(UbiKey.UNIT.toString()).toString());
				}
			}catch(Exception e ){
				e.printStackTrace();
				return null;}
			
		}
		
		return vs;
		
	}

	@Override
	public boolean isSystemReadyToUse() {
	
		if (this.getSystemRoot()!=null)return true;
		else return false;
	}
	
	
	private boolean isInValueSet(String urn,String valueSetUrn){
		Concept myconcept=null;
		try {
			myconcept=MyResource.services.getConcept(UbiKey.URN.toString(),urn,"ALL");
			Iterator<Property> ite=myconcept.getProperties().iterator();
			while (ite.hasNext()){
				Property p=ite.next();
				if (p.getName().equalsIgnoreCase(UbiKey.HASVALUESET.toString())){
					if (p.getValue().equalsIgnoreCase(valueSetUrn))return true;
				}
			}
			if (myconcept.getParents()!=null){
				for (ErsatzConcept ec:myconcept.getParents()){
					if (this.isInValueSet(ec.getUrn(), valueSetUrn)){
						return true;
					}
				}
			}else return false;
		
		}catch (Exception e ){
			e.printStackTrace();
			return false;
			
		}
		return false;
	}
	
	
	private void addChildren(LevelValue levelValue){
		boolean All=false;
		
		System.out.println(levelValue.getExternalId());
		Concept ec=null;
		try {ec=MyResource.services.getConcept(UbiKey.EXTERNAL_ID.toString(), levelValue.getExternalId(),levelValue.getTerminologyUrn());
		}catch (Exception e ){
			e.printStackTrace();
			System.err.println(levelValue.getExternalId()+"  unkown...");
			return;
		}
		this.fillLevelValue(ec, levelValue); // on enrichit notre value par des properties from Central
		
		if (levelValue.getLevel()<0)return;
		if (levelValue.getLevel()==1)return;
		
		int level=levelValue.getLevel();
		Set<ErsatzConcept> children=ec.getChildren();
		if (children.isEmpty())return;
		
		for (ErsatzConcept child:children){
			int newLevel=0;
			if (level!=0)newLevel=level-1;
			LevelValue lv=new LevelValue(newLevel);
			this.fillLevelValue(child, lv);
			this.addChildren(lv);
			levelValue.getChildren().add(lv);
		}
		
	}
	
	private void fillLevelValue(AbstractConcept ec,LevelValue lv){
		lv.setExternalId(ec.getId());
		lv.setLabel(ec.getLabel());
		lv.setUrn(ec.getUrn());
		for (Property p:ec.getProperties()){
			if (p.getName().equalsIgnoreCase("CodingSchemeURI")){
				lv.setTerminologyUrn(p.getValue());
			}
			if (p.getName().equalsIgnoreCase("CodingSchemeName")){
				lv.setTerminologyName(p.getValue());
			}
		}
	}*/
	
}
