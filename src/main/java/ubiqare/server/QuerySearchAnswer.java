package ubiqare.server;

import java.util.ArrayList;
import java.util.List;

import ubiqore.patterns.concept.ErsatzConcept;

public class QuerySearchAnswer {
	String message="no answer to your request";
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	long totalHit=0;
	public long getTotalHit() {
		return totalHit;
	}
	public void setTotalHit(long totalHit) {
		this.totalHit = totalHit;
	}
	public List<ErsatzConcept> getErsatzConcepts() {
		return ersatzConcepts;
	}
	public void setErsatzConcepts(List<ErsatzConcept> ersatzConcepts) {
		this.ersatzConcepts = ersatzConcepts;
	}
	List<ErsatzConcept> ersatzConcepts=new ArrayList<ErsatzConcept>();
}
