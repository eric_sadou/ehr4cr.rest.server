package ubiqare.server;
 
import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import javax.ws.rs.QueryParam;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;

import ehr4cr.rest.api.Services;
import ehr4cr.rest.context.data.Ehr4crElement;
import ehr4cr.rest.definitions.RestTools;

import ubiqare.server.centrale.imp.EntityNameOrUri;
import ubiqare.server.centrale.imp.ServicesImpl;
import ubiqare.server.centrale.imp.Term;
import ubiqare.server.centrale.imp.ValueSetServicesImpl;

import ubiqore.patterns.concept.Concept;
import ubiqore.patterns.concept.ErsatzConcept;
import ubiqore.patterns.concept.SimpleConcept;

/**
 * Example resource class hosted at the URI path "/myresource"
 */

/**
 * @author roky
 *
 */
public class MyResource {
	/*@Context
	private HttpServletResponse response;

	
	
	public static Services services = (Services) new ServicesImpl();
	//public static ValueSetServices vsServices = new ValueSetServicesImpl();
	String id;
	*//**
	 * Method processing HTTP GET requests, producing "text/plain" MIME media
	 * type.
	 * 
	 * @return String that will be send back as a response of type "text/plain".
	 *//*
	@GET
	@Produces("text/plain")
	public String getIt() {
		return "Hi there! "+id;
	}
	
	@GET
	@Produces("text/plain")
	@Path("/hello/{id}")
	public String hello(@PathParam("id") String id) {
		return "Hi there! "+id;
	}
	

	@GET
	// @Produces("application/vnd.ms-excel")
	@Produces("application/*")
	@Path("/getCodesForMapping")
	public Response getCodesForMapping(@DefaultValue("	") @QueryParam("separ") String separ,@QueryParam("type")  String type,@QueryParam("sublevels")  int sublevels) {
		char separc=separ.charAt(0);
		File rep = services.getCodesForMapping(separc,type,sublevels);
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponseForFile(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getCodesForMapping")
	public Response getCodesForMappingPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	
	@GET
	// @Produces("application/vnd.ms-excel")
	@Produces("application/*")
	@Path("/exportRDF")
	public Response exportRDF() {
		File rep = services.exportXMLRDF();
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponseForFile(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/exportRDF")
	public Response exportRDFPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/getCategories")
	public Response getCategories() {
		Ehr4crElement categoryElement = services.getRoot();
		if (categoryElement == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(categoryElement);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getCategories")
	public Response getCategoriesPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/connect")
	public Response connect(@QueryParam("pass") String pass ) {
		String rep=services.connect(pass);
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/connect")
	public Response connectPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/disconnect")
	public Response disconnect(@QueryParam("pass") String pass) {
		String rep=services.disconnect(pass);
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/disconnect")
	public Response disconnectPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	
	@GET
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/getCTS2DataElement")
	public Response getCTS2DataElement(JAXBElement<EntityNameOrUri> entityNameOrUri) {
		Ehr4crElement rep = services.getDataElement(entityNameOrUri.getValue());
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getCTS2DataElement")
	public Response getCTS2DataElementPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/getDataElement")
	public Response getDataElement(@QueryParam("uri") String uri) {
		Ehr4crElement rep = services.getDataElement(uri);
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getDataElement")
	public Response getDataElementPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	

	@GET
	@Produces("application/json")
	@Path("/getTermExpansion")
	public Response getTermExpansion(@QueryParam("uri") String uri, @QueryParam("limit") String limit ) {
		Term rep=null;
		try {
			int level=new Integer(limit);
			rep = services.getTermExpansion(uri,level); 
		}catch (Exception e){
			rep=null;
		}
		
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getTermExpansion")
	public Response getTermExpansionPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/searchDataElement")
	public Response searchDataElement(@QueryParam("str") String str ) {
		List<Ehr4crElement> rep=services.searchDataElement(str);
		
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/searchDataElement")
	public Response searchValueSetElementPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/getCategories")
	public Response getCategories() {
		HashSet<CategoryValueSet> rep = vsServices.getCategories();
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getCategories")
	public Response getCategoriesPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}

	@GET
	@Produces("application/json")
	@Path("/getCategoryValueSet")
	public Response getCategoryValueSet(@QueryParam("key") String key,
			@QueryParam("value") String value) {
		CategoryValueSet rep = vsServices.getCategoryValueSet(key, value);
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getCategoryValueSet")
	public Response getCategoryValueSetPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}

	@GET
	@Produces("application/json")
	@Path("/getFullValueSet")
	public Response getFullValueSet(@QueryParam("urn") String urn) {
		ValueSet rep = vsServices.getFullValueSet(urn);
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getFullValueSet")
	public Response getFullValueSetPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}

	@GET
	@Produces("application/json")
	@Path("/getLevelValue")
	public Response getLevelValue(@QueryParam("key") String key,
			@QueryParam("value") String value,@DefaultValue("0") @QueryParam("level") String level) {
		try {
			new Integer(level).intValue();
		}catch (Exception e ){RestTools.createNoResponse(); }
		
		LevelValue rep = vsServices.getLevelValue(key, value, new Integer(level).intValue());
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getLevelValue")
	public Response getLevelValuePreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/getSystemRoot")
	public Response getSystemRoot() {
		CategoryValueSet rep = vsServices.getSystemRoot();
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getSystemRoot")
	public Response getSystemRootPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	
	@GET
	@Produces("application/json")
	@Path("/getValueSetVersion")
	public Response getValueSetVersion() {
		String rep = vsServices.getVSSVersion();
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getValueSetVersion")
	public Response getValueSetVersionPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/getValueSet")
	public Response getValueSet(@QueryParam("key") String key,
			@QueryParam("value") String value, @DefaultValue("0") @QueryParam("level")String level) {
		try {
			new Integer(level).intValue();
		}catch (Exception e ){RestTools.createNoResponse(); }
		
		ValueSet rep = vsServices.getValueSet(key, value, new Integer(level).intValue());
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getValueSet")
	public Response getValueSetPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}

	@GET
	@Produces("application/json")
	@Path("/getConcept")
	public Response getConcept(@QueryParam("key") String key,
			@QueryParam("value") String value,@DefaultValue("ALL")
			@QueryParam("codingSchemeURI") String codingSchemeURI) {

		Concept c = services.getConcept(key, value, codingSchemeURI);
		if (c == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(c);
	}
	

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getConcept")
	public Response getConceptPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
 *
	@GET
	@Produces("application/json")
	@Path("/getErsatzConcept")
	public Response getErsatzConcept(@QueryParam("key") String key,
			@QueryParam("value") String value,@DefaultValue("ALL")
			@QueryParam("codingSchemeURI") String codingSchemeURI) {

		ErsatzConcept c = services
				.getErsatzConcept(key, value, codingSchemeURI);
		if (c == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(c);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getErsatzConcept")
	public Response getErsatzConceptPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/luckySearch")
	public Response luckySearch(@QueryParam("search") String st,@DefaultValue("ALL")
			@QueryParam("codingSchemeURI") String codingSchemeURI) {
		Concept concept = services.luckySearch(st, codingSchemeURI);
		if (concept == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(concept);

	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/luckySearch")
	public Response luckySearchPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}

	@GET
	@Produces("application/json")
	@Path("/searchConcept")
	public Response searchConcept(@QueryParam("search") String st,@DefaultValue("ALL")
			@QueryParam("codingSchemeURI") String codingSchemeURI) {

		List<ErsatzConcept> c = services.searchConcept(st, codingSchemeURI);
		if (c == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(c);
		
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/searchConcept")
	public Response searchConceptPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/searchConceptWithLimit")
	public Response searchConceptWithLimit(@QueryParam("search") String st,@DefaultValue("ALL")
			@QueryParam("codingSchemeURI") String codingSchemeURI,@QueryParam("limit") int limit,@QueryParam("from") int from) {

		QuerySearchAnswer rep = services.searchConceptWithLimit(st, codingSchemeURI,limit,from);
		if (rep == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
		
	} 

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/searchConceptWithLimit")
	public Response searchConceptWithLimitPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	@GET
	@Produces("application/json")
	@Path("/getSubConceptsList")
	public Response getSubConceptsList(
			@QueryParam("key") String key,
			@QueryParam("value") String value,
			@DefaultValue("ALL") @QueryParam("codingSchemeURI") String codingSchemeURI,
			@QueryParam("level") int level) {

		Set<SimpleConcept> set = services.getSubConceptsList(key, value, codingSchemeURI, level);
		if (set == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(set);
		
	} 

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getSubConceptsList")
	public Response getSubConceptsListPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	
	
	
	@GET
	@Produces("application/json")
	@Path("/searchConceptByKey")
	public Response searchConceptByKey(@QueryParam("key") String key,@DefaultValue("false") @QueryParam("value") String value,
			@QueryParam("codingSchemeURI") String codingSchemeURI) {

		List<ErsatzConcept> c = services.searchConceptByKey(key,value, codingSchemeURI);
		if (c == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(c);
		
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/searchConceptByKey")
	public Response searchConceptByKeyPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}

	@GET
	@Produces("application/json")
	@Path("/getBranches")
	public Response getBranches(
			@QueryParam("codingSchemeURI") String codingSchemeURI) {
		Set<ErsatzConcept> branches = services.getBranches(codingSchemeURI);
		if (branches == null)
			return RestTools.createNoResponse();
		return RestTools.createResponse(branches);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/getBranches")
	public Response getBranchesPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	
	
	
	
	
	@POST
	@Produces("application/json")
	@Path("/addValueSet")
	public Response addValueSet(
			@QueryParam("conceptURN") String conceptUrn,@QueryParam("valueSetURN") String valueSetUrn, @QueryParam("replace") String replace) {
		boolean replaceB=false;
		
		if (replace.equalsIgnoreCase("TRUE")||replace.equalsIgnoreCase("1"))replaceB=true;
		
		boolean rep=services.linkConceptToValueSet(conceptUrn, valueSetUrn,replaceB);
		if(rep==false)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/addValueSet")
	public Response addValueSetPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	
	
	@POST
	@Produces("application/json")
	@Path("/addUnit")
	public Response addUnit(
			@QueryParam("conceptURN") String conceptUrn,@QueryParam("unitURN") String unitUrn, @QueryParam("replace") String replace) {
		boolean replaceB=false;
		
		if (replace.equalsIgnoreCase("TRUE")||replace.equalsIgnoreCase("1"))replaceB=true;
		
		boolean rep=services.linkConceptToUnit(conceptUrn, unitUrn,replaceB);
		if(rep==false)
			return RestTools.createNoResponse();
		return RestTools.createResponse(rep);
	}

	// The response for the preflight request made implicitly by the browser
	@OPTIONS
	@Path("/addUnit")
	public Response addUnitPreflight() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"x-http-method-override, X-Requested-With").build();

	}
	

	public static void main(String... args) {
		MyResource test = new MyResource();
		test.getCategories();
		System.out.println(((Concept) test.getConcept("URN",
				"urn:uuid:bdc24cb0-0b06-4925-890e-470fccad2c04", Services.ALL)
				.getEntity()).toString());
	//	test.searchConcept("Male", Services.ALL);
	//	test.addValueSet("urn:uuid:bdc24cb0-0b06-4925-890e-470fccad2c04", "toto",true);
	//	test.addValueSet("urn:uuid:bdc24cb0-0b06-4925-890e-470fccad2c04", "urn:uuid:d4153050-b632-4133-86af-d9455c532e59",true);
	//	List<ErsatzConcept> l=test.services.searchConceptByKey(UbiKey.HASVALUESET.toString(),"urn:uuid:d4153050-b632-4133-86af-d9455c532e59", Services.ALL);
		
		
		
		//	if (!l.isEmpty())System.out.println(l.get(0).getLabel());
	//	System.out.println(test.getErsatzConcept("EXTERNAL_ID", "A", "atc.2012").getEntity().toString());
	}*/
}
